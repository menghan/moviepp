pylint:
	@pylint -E pp/ | egrep -v "((has no '(orm|cache|query)' member)|(Instance of '(CustomSQLAlchemy|SessionBackendProxy|CustomManager|CustomCommand|ProfilerMiddleware)' has no ))"

build_db_imports:
	@git grep -l 'db.Model' -- pp | sed -e 's/\//./g' -e 's/\.py//' -e 's/^/from /' -e 's/$$/ import */' | grep -v testmodel > pp/manage/dbmodels.py
