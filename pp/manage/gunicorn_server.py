import os
import sys

from gunicorn.config import make_settings
from flask_script import Command, Option  # pylint: disable=no-name-in-module


class GunicornServer(Command):
    """Run the app within Gunicorn"""

    def get_options(self):
        settings = make_settings()
        options = (
            Option(*klass.cli, action=klass.action)
            for setting, klass in settings.iteritems() if klass.cli
        )
        return options

    def run(self, *args, **kwargs):
        run_args = sys.argv[2:]
        run_args.append('pp.app:flask_app')
        os.execvp('gunicorn', [''] + run_args)
