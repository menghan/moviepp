# coding=utf-8

from datetime import datetime, timedelta

from pp.lib.sqlstore import db
from pp.model.show.models import movies
from pp.manage.manager import manager


@manager.command
def sort_movies():
    """sort movies by show_count"""
    yesterday = (datetime.now() - timedelta(days=1)).date()
    sql = "update movie inner join (select movie_id, count(1) as show_count from `show` where `show`.date >= '{}' group by movie_id) as movie_count on movie.id = movie_count.movie_id set movie.show_count = movie_count.show_count".format(yesterday)
    db.engine.execute(sql)
    movies.update_onshow_movies()
