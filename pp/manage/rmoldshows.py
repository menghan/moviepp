# coding=utf-8

import time
from datetime import datetime, timedelta

from pp.lib.sqlstore import db
from pp.model.show.models import shows
from pp.manage.manager import manager


@manager.option('--year')
@manager.option('--month')
@manager.option('--day')
def rmoldshows(year, month, day):
    date = datetime(int(year), int(month), int(day)).date()
    sql = 'delete from `show` where date = %s' % date
    r = db.engine.execute(sql).execution_options(autocommit=True)
    print repr(r)
