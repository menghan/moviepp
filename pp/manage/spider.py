# coding=utf-8

from datetime import datetime, timedelta
import json
import time

from pyquery import PyQuery as pq

from pp.lib.sqlstore import db
from pp.lib.crawler import capture, logger, interval
from pp.lib.crawler import crawl_meituan_onshow, crawl_meituan_schedules
from pp.model.location.models import locations
from pp.model.show.models import cinemas, shows, movies
from pp.manage.manager import manager


@manager.command
def fetch_meituan_onshow():
    """fetch onshow movie details from meituan"""
    crawl_meituan_onshow.async()()


@manager.option('--hot', help='Only list hot cities')
def list_meituan_cities(hot):
    db.engine.echo = False
    if hot and hot not in ('false', '0'):
        cities = locations.hot_cities()
    else:
        cities = locations.all_cities()
        if hot in ('false', '0'):
            cities = [c for c in cities if c not in locations.hot_cities()]
    cities = filter(lambda c: c.mt_abbv != '', cities)
    for city in cities:
        print city.name.encode('utf8')


@manager.option('--city', dest='city', required=True, help='Citiy name')
def fetch_meituan_cinemas(city):
    """fetch cinemas from meituan"""
    db.engine.echo = False
    city = unicode(city, 'utf8')
    c = locations.search_city(city)
    if not c:
        logger.critical('can\'t find city %s', city)
        return
    if not c.mt_abbv:
        logger.critical('can\'t find city\'s meituan abbv %s', c.name)
        return
    logger.info('capturing cinemas for city %s, which has %d cinemas', c.name, len(cinemas.findall(loc_id=c.id)))
    cinema_sum = 0
    for i in xrange(1, 100):
        cinema_addrs = {}
        cinema_ids = {}
        d = capture('http://%s.meituan.com/dianying/cinemalist/all/all/page%s?mtt=1.movie/movies.0.0.ii1ls4rq' % (c.mt_abbv, i))
        for cinema in d('.J-cinema-item'):
            j = json.loads(cinema.get('data-params'))
            cinema_addrs[j['nm']] = j['addr']
        for cinema in d('.link--black__green'):
            id = int(cinema.get('href', '').rsplit('/')[-1])
            name = cinema.text
            cinema_ids[name] = id
        assert len(cinema_ids) == len(cinema_addrs)
        for name, addr in cinema_addrs.iteritems():
            id = cinema_ids[name]
            cinema = cinemas.first(mt_id=int(id))
            if not cinema:
                cinema = cinemas.create(
                    name=name,
                    addr=addr,
                    mt_id=id,
                    loc_id=c.id,
                )
                logger.info('import new cinema mt_id=%s name=%s', id, name)
            else:
                logger.info('cinema mt_id=%s to be updated', int(id))
                changed = False
                if cinema.addr != addr:
                    cinema.addr = addr
                    changed = True
                if cinema.loc_id != c.id:
                    cinema.loc_id = c.id
                    changed = True
                if name != cinema.name:
                    cinema.name = name
                    changed = True
                if changed:
                    cinema.orm.save(cinema)
        cinema_sum += len(cinema_addrs)
        if len(cinema_addrs) < 20:
            logger.info('page %d of city %s has only %d cinemas, breaking', i, c.name, len(cinema_addrs))
            break
        time.sleep(interval)
    logger.info('city %s has %d cinemas', c.name, len(cinemas.findall(loc_id=c.id)))
    if len(cinemas.findall(loc_id=c.id)) < cinema_sum:
        logger.error('failed to save city %s\'s cinemas', c.name)


@manager.option('--city', help='Only list cinemas from the city', required=False)
@manager.option('--full', help='show full cinema attributes', required=False)
def list_meituan_cinemas(city, full):
    db.engine.echo = False
    if city:
        city = unicode(city, 'utf8')
        c = locations.search_city(city)
        if not c or c.mt_abbv == '':
            logger.critical('can\'t find meituan city %s', city)
            return
        cs = cinemas.findall(loc_id=c.id)
    else:
        cs = cinemas.findall()
    for cinema in cs:
        if not full:
            print cinema.name.encode('utf8')
        else:
            print '\t'.join([cinema.name.encode('utf8'), cinema.loc.name.encode('utf8'), cinema.addr.encode('utf8')])


@manager.option('--city', help='Only list cinemas from the city')
@manager.option('--filterhot', dest='filter_hot', help='filter only hot cities\' cinemas')
@manager.option('--filterhasschedule', dest='filter_hasschedule', help='filter only has schedule cinemas')
def list_meituan_cinemas_sorted(city, filter_hot, filter_hasschedule):
    db.engine.echo = False
    if city:
        city = unicode(city, 'utf8')
        c = locations.search_city(city)
        if not c or c.mt_abbv == '':
            logger.critical('can\'t find meituan city %s', city)
            return
    else:
        c = None

    hot_city_ids = [l.id for l in locations.hot_cities()]
    yesterday = (datetime.now() - timedelta(days=1)).date()
    yesterday_cinema_ids = set([s.cinema_id for s in shows.query.filter_by(date=yesterday).with_entities(shows.cinema_id).distinct()])

    cs = cinemas.findall()
    cs.sort(key=lambda c: (c.loc_id in hot_city_ids, c.id in yesterday_cinema_ids), reverse=True)

    if c:
        cs = [c for c in cs if c.loc_id == c.id]
    elif filter_hot:
        if filter_hot == '0':
            cs = [c for c in cs if c.loc_id not in hot_city_ids]
        elif filter_hot:
            cs = [c for c in cs if c.loc_id in hot_city_ids]
    if filter_hasschedule:
        if filter_hasschedule == '0':
            cs = [c for c in cs if c.id not in yesterday_cinema_ids]
        elif filter_hasschedule:
            cs = [c for c in cs if c.id in yesterday_cinema_ids]

    for c in cs:
        print c.mt_id


@manager.option('--mt_id', dest='mt_id', help='Cinema\'s meituan id')
def mt_id_to_cinema_name(mt_id):
    db.engine.echo = False
    cinema = cinemas.first(mt_id=int(mt_id))
    print cinema.name.encode('utf8')


@manager.option('--mt_id', dest='mt_id', help='Cinema\'s meituan id(s)')
def fetch_meituan_schedules(mt_id):
    """ import meituan schedules """
    db.engine.echo = False
    for id in mt_id.strip().split():
        cinema = cinemas.first(mt_id=int(id))
        if not cinema:
            logger.critical('can\'t find cinema mt_id=%s', mt_id)
            continue
        crawl_meituan_schedules.async()(cinema.id)


@manager.option('--mt_id', dest='mt_id', help='Cinema\'s meituan id')
@manager.option('--captured', dest='captured', help='pre saved captured html')
@manager.option('--sleep', dest='sleep', help='should sleep or not')
def test_fetch_schedules(mt_id, captured, sleep):
    """testing import meituan schedules """
    db.engine.echo = False
    cinema = cinemas.first(mt_id=int(mt_id))
    if not cinema:
        logger.critical('can\'t find cinema mt_id=%s', mt_id)
        return
    crawl_meituan_schedules(cinema.id, d=pq(open(captured).read()), sleep=(sleep != '0'))


@manager.option('--movie_mt_id', help='meituan\'s movie id')
@manager.option('--intro', help='movie intro')
def update_movie_intro(movie_mt_id, intro):
    '''update movie intro'''
    db.engine.echo = False
    movie = movies.first(mt_id=int(movie_mt_id))
    if not movie:
        logger.critical('can\'t find movie mt_id=%s', movie_mt_id)
        return
    movie.intro = intro.decode('utf8')
    movie.orm.save(movie)
