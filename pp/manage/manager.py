import os
import pkgutil
import importlib

from flask_script import Manager, Command, Option, prompt  # pylint: disable=no-name-in-module
from flask_migrate import Migrate, MigrateCommand

from pp.manage.gunicorn_server import GunicornServer


class CustomCommand(Command):

    def __call__(self, app=None, *args, **kwargs):
        """
        Handles the command with the given app.
        Default behaviour is to call ``self.run`` within a test request context.
        """
        with app.test_request_context():
            app.preprocess_request()
            return self.run(*args, **kwargs)


class CustomManager(Manager):

    def command(self, func):
        command = CustomCommand(func)
        self.add_command(func.__name__, command)
        return func

    def option(self, *args, **kwargs):
        option = Option(*args, **kwargs)

        def decorate(func):
            name = func.__name__

            if name not in self._commands:

                command = CustomCommand(func)
                command.__doc__ = func.__doc__
                command.option_list = []

                self.add_command(name, command)

            self._commands[name].option_list.append(option)
            return func
        return decorate

manager = CustomManager()


def main():
    for _, name, _ in pkgutil.iter_modules([os.path.dirname(os.path.abspath(__file__))]):
        importlib.import_module('pp.manage.' + name)

    manager.add_command('db', MigrateCommand)
    manager.add_command('runserver', GunicornServer())

    from pp.app import flask_app as app
    from pp.lib.sqlstore import db
    migrate = Migrate()
    migrate.init_app(app, db)
    manager.app = app
    manager.run()
