# coding=utf-8

import sys
from os.path import join, dirname, abspath
import subprocess
import json

import redis
from flask import current_app
from sqlalchemy.ext.serializer import dumps, loads

from pp.lib.sqlstore import db
from pp.manage.manager import manager
from pp.manage import dbmodels


@manager.command
def initdb():
    """init db"""
    rediscli = redis.from_url(current_app.config['CACHE_REDIS_URI'])
    rediscli.flushdb()
    db.drop_all()
    db.create_all()
    db.session.commit()
    current_app.logger.info('Tables created at %s', db)
    migrations_path = join(dirname(dirname(dirname(abspath(__file__)))), 'migrations')
    cmd = 'manage db heads -d %s' % migrations_path
    current_ver = subprocess.check_output(cmd.split()).splitlines()[0].split()[0]
    cmd = 'manage db stamp %s -d %s' % (current_ver, migrations_path)
    subprocess.check_call(cmd.split())


@manager.option('-s', '--source', dest='source',
                default='serialized_dump.txt',
                required=False, help='Restore fixture from dump')
def restore(source):
    print >> sys.stderr, 'Start importing data from %s' % source
    with (sys.stdin if source == '-' else open(source)) as f:
        data = json.loads(f.readline())
    for model_data in data:
        try:
            restored = loads(model_data, db.metadata, db.session)
        except AttributeError as e:
            print >> sys.stderr, 'Table does not exist: {}'.format(e)
            continue
        if restored:
            print >> sys.stderr, 'Importing {} table...'.format(restored[0].__table__.name)
        for item in restored:
            db.session.merge(item)

        db.session.commit()

    print >> sys.stderr, 'Done'


@manager.option('-d', '--destination', required=True, help='Output file')
@manager.option('-m', '--models', required=True, help='Dumped models, seperated by ","')
def dump(destination, models):
    db.engine.echo = False
    dump_models = [getattr(dbmodels, model) for model in models.split(',')]
    serialized = list()
    for model in dump_models:
        print >> sys.stderr, 'Dumping {} to {}'.format(model, destination)
        serialized.append(unicode(dumps(db.session.query(model).all()), errors='ignore'))
    with (sys.stdout if destination == '-' else open(destination, 'wb')) as f:
        f.writelines(json.dumps(serialized))
