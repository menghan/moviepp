# coding=utf-8

import sys
from os.path import join, dirname
from datetime import date, timedelta, datetime
import json
import gzip

from sqlalchemy.ext.serializer import dumps, loads
from flask import current_app

from pp.lib.sqlstore import db
from pp.manage.manager import manager
from pp.model.user.models import phone_register, phone_login, phone_reset_password
from pp.model.user.verifycode import VerifyCode
from pp.model.oauth2.models import clients
from pp.model.show.models import movies, cinemas, shows, tickets
from pp.model.location.models import locations, LocType
from pp.lib.pinyin import hanzi2pinyin


def add_dev_users():
    """Add development environment users"""
    for phone in ['13800138000', '13988888888', '18666666666']:
        code = VerifyCode.generate(phone)
        phone_register(phone, 'pass', code)
        phone_login(phone, 'pass')
        code = VerifyCode.generate(phone)
        phone_reset_password(phone, 'newpass', code)
        phone_login(phone, 'newpass')


def add_locations():
    locfile = join(dirname(__file__), 'location.data.gz')
    data = json.loads(gzip.GzipFile(locfile).readline())
    for model_data in data:
        try:
            restored = loads(model_data, db.metadata, db.session)
        except AttributeError as e:
            print >> sys.stderr, 'Table does not exist: {}'.format(e)
            continue
        if restored:
            print >> sys.stderr, 'Importing {} table...'.format(restored[0].__table__.name)
        for item in restored:
            db.session.merge(item)

        db.session.commit()


def add_dev_movies():
    for name in [u'霸王别姬', u'阿甘正传']:
        movies.create(
            name=name,
            intro='this is intro for %s' % name,
            rating=80,
            attrs='tags:美国/剧情',
            en_name='Ba wang bie ji',
        )


def add_dev_cinemas():
    for loc_name, name in [(u'北京', u'华星UME'), (u'上海', u'保利大剧院')]:
        loc = locations.search_city(loc_name)
        cinemas.create(
            loc_id=loc.id,
            name=name,
            tel='13800138000',
        )


def add_dev_shows():
    show = shows.create(
        date=date.today(),
        time=date.today() + timedelta(hours=18),
        movie_id=1,
        cinema_id=1,
        hall=u'1号厅',
    )


def add_dev_tickets():
    show = shows.get(1)
    tickets.create(
        hall=show.hall,
        seats=u'1排A座,A排1座',
        price=5000,
        fee=300,
        retailer_id=1,
        user_id=1,
        show_id=show.id,
    )


def add_dev_official_client():
    clients.create_ignore(
        client_id=current_app.config['OFFICIAL_CLIENT_ID'],
        client_secret=current_app.config['OFFICIAL_CLIENT_SECRET'],
        _default_scopes=' '.join(current_app.config['OFFICIAL_SCOPES']),
        _redirect_uris='/',
    )
    clients.create_ignore(
        client_id=current_app.config['OFFICIAL_IOS_CLIENT_ID'],
        client_secret=current_app.config['OFFICIAL_IOS_CLIENT_SECRET'],
        _default_scopes=' '.join(current_app.config['OFFICIAL_SCOPES'] + ['ios']),
        _redirect_uris='/',
    )
    clients.create_ignore(
        client_id=current_app.config['OFFICIAL_ANDROID_CLIENT_ID'],
        client_secret=current_app.config['OFFICIAL_ANDROID_CLIENT_SECRET'],
        _default_scopes=' '.join(current_app.config['OFFICIAL_SCOPES'] + ['android']),
        _redirect_uris='/',
    )


@manager.command
def initdata():
    """init some basic data into database"""
    add_dev_official_client()


@manager.command
def initlocations():
    add_locations()


@manager.command
def initmoredata():
    """init some more data into database"""
    add_dev_users()
    add_dev_movies()
    add_dev_cinemas()
    add_dev_shows()
    add_dev_tickets()
