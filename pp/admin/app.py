import os

import flask_admin
from flask_admin.contrib import sqla

from pp.factory import create_app as create_app_base


def create_app(settings_override=None):
    package_name = __name__.rsplit('.', 1)[0]  # rstrip the '.app'
    app = create_app_base(package_name, settings_override)
    admin = flask_admin.Admin(app, name='Movieplusplus', template_mode='bootstrap3', url='/')
    init_admin_with_models(admin)
    return app


class CustomAdmin(sqla.ModelView):
    can_edit = False
    can_create = False
    can_delete = False
    column_default_sort = 'id', True


def truncate(s, l):
    is_unicode = isinstance(s, unicode)
    if not is_unicode:
        s = str(s).decode('utf8')
    if len(s) > l:
        s = s[:l] + '...'
    if not is_unicode:
        s = s.encode('utf8')
    return s


class ReportAdmin(CustomAdmin):
    column_list = ['id', 'user_id', 'content', 'created_at']
    column_sortable_list = column_list
    column_labels=dict(user_id='User')
    can_view_details = True
    details_modal = False
    column_formatters = dict(
        user=lambda view, context, model, name: model.user and model.user.nickname,
        content=lambda view, context, model, name: truncate(model.content, 40) if 'details_columns' not in context else model.content,
    )


class UserAdmin(CustomAdmin):
    column_list = ['id', 'nickname', 'gender']
    column_searchable_list = ['id', 'nickname']
    column_sortable_list = ['id']
    column_filters = ['gender']
    column_formatters = dict(
        # favloc=lambda view, context, model, name: model.favloc and model.favloc.name,
        gender=lambda view, context, model, name: model.gender_human_str,
    )


class CinemaAdmin(CustomAdmin):
    can_edit = True
    edit_modal = True
    column_list = ['id', 'name', 'tel', 'addr']
    column_searchable_list = ['id', 'name', 'tel', 'addr']
    column_sortable_list = ['id']
    # column_filters = ['loc']
    # column_formatters = dict(
    #     loc=lambda view, context, model, name: model.loc and model.loc.name,
    # )
    form_excluded_columns = ['id', 'loc_id', 'mt_id']


class MovieAdmin(CustomAdmin):
    can_edit = True
    edit_modal = True
    column_list = ['id', 'name', 'pub_date', 'rating']
    column_searchable_list = ['id', 'name', 'en_name', 'pub_date', 'year', 'intro', 'attrs']
    column_sortable_list = ['id', 'pub_date', 'year']
    form_excluded_columns = ['id', 'rating', 'mt_id', 'mt_cover', 'show_count']


class TicketAdmin(CustomAdmin):
    column_list = ['id', 'hall', 'seats', 'created_at']
    column_searchable_list = ['id']
    column_sortable_list = ['id']
    # column_formatters = dict(
    #     user=lambda view, context, model, name: model.user and model.user.nickname,
    # )


def init_admin_with_models(admin):
    from pp.model.report.models import Report
    from pp.model.user.models import User
    from pp.model.show.models import Ticket, Movie, Cinema
    from pp.lib.sqlstore import db
    admin.add_view(ReportAdmin(Report, db.session))
    admin.add_view(UserAdmin(User, db.session))
    admin.add_view(TicketAdmin(Ticket, db.session))
    admin.add_view(MovieAdmin(Movie, db.session))
    admin.add_view(CinemaAdmin(Cinema, db.session))
