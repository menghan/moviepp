# coding=utf-8

from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.contrib.fixers import ProxyFix
from werkzeug.debug import DebuggedApplication

from pp.lib.profiler import ProfilerMiddleware
from pp.factory import create_app
from pp.frontend.app import create_app as create_frontend_app
from pp.admin.app import create_app as create_admin_app
from pp.api.app import create_app as create_api_app
from pp.internal.app import create_app as create_internal_app

flask_app = create_app(__name__)

frontend_app = create_frontend_app()
api_app = create_api_app()
admin_app = create_admin_app()
internal_app = create_internal_app()
flask_app.wsgi_app = DispatcherMiddleware(frontend_app.wsgi_app, {
    '/api': api_app.wsgi_app,
    '/admin': admin_app.wsgi_app,
    '/internal': internal_app.wsgi_app,
})
if flask_app.debug:
    use_evalex = True
    flask_app.wsgi_app = DebuggedApplication(flask_app.wsgi_app, use_evalex)

flask_app.wsgi_app = ProfilerMiddleware(flask_app.wsgi_app, profile_dir='/tmp')
flask_app.wsgi_app = ProxyFix(flask_app.wsgi_app)
