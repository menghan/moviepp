from flask_login import LoginManager
from flask_restful import Api
from flask_restful_swagger import swagger

from pp.factory import create_app as create_app_base
import pp.lib.exc as exc
from pp.api.utils import patch_api_resource, before_request_funcs, after_request_funcs, patch_marshal, patch_string_output
from pp.api.oauth2 import oauth2_provider
from pp.lib.qiniu import cdn_storage

api = Api(prefix='/v1', errors=exc.api_errors)
api = swagger.docs(api, apiVersion='v1', api_spec_url='/spec')
api = patch_api_resource(api)

patch_marshal()
patch_string_output()


def unauthorized_handler():
    raise exc.NotLoginError


def create_app(settings_override=None):
    package_name = __name__.rsplit('.', 1)[0]  # rstrip the '.app'
    app = create_app_base(package_name, settings_override)
    for func in before_request_funcs:
        app.before_request(func)
    for func in after_request_funcs:
        app.after_request(func)

    from pp.api.resources import load_sub_modules
    load_sub_modules()

    cdn_storage.init_app(app)

    login_manager = LoginManager(app)
    login_manager.request_loader(lambda request: request.user)
    login_manager.unauthorized_handler(unauthorized_handler)
    oauth2_provider.init_app(app)
    api.init_app(app)

    return app
