import sys
import functools
import json

from flask import request, current_app
from flask_login import login_required as view_login_required
from flask_restful_swagger import swagger
import flask_restful
import flask_restful.fields
from flask_restful import (
    Resource as OriginResource,
    reqparse, abort, fields
)
from werkzeug.datastructures import FileStorage

import pp.lib.exc as exc
from pp.lib.logger import applog
from pp.api.oauth2 import oauth2_provider
from pp.model.location.models import locations
from pp.model.user.loc import set_user_recent_lat_lng


def login_required(func):
    f = functools.wraps(func)(
        view_login_required(
            func
        )
    )
    if hasattr(func, 'marshal_model'):
        f.marshal_model = func.marshal_model
    f.login_required = True
    return f


def marshal(data, fields, envelope=None):
    """Takes raw data (in the form of a dict, list, object) and a dict of
    fields to output and filters the data based on those fields.

    :param data: the actual object(s) from which the fields are taken from
    :param fields: a dict of whose keys will make up the final serialized
                   response output
    :param envelope: optional key that will be used to envelop the serialized
                     response


    >>> data = { 'a': 100, 'b': 'foo' }
    >>> mfields = { 'a': fields.Raw }

    >>> marshal(data, mfields)
    dict([('a', 100)])

    >>> marshal(data, mfields, envelope='data')
    dict([('data', dict([('a', 100)]))])

    """

    def make(cls):
        if isinstance(cls, type):
            return cls()
        return cls

    if isinstance(data, (list, tuple)):
        return (dict([(envelope, [marshal(d, fields) for d in data])])
                if envelope else [marshal(d, fields) for d in data])

    items = ((k, marshal(data, v) if isinstance(v, dict)
              else make(v).output(k, data))
             for k, v in fields.items())
    return dict([(envelope, dict(items))]) if envelope else dict(items)


def patch_marshal():
    flask_restful.marshal = marshal
    flask_restful.fields.marshal = marshal


def patched_marshal_with_add_code(fields, envelope=None):
    def _wrapper(f):
        @functools.wraps(f)
        def _(*a, **kw):
            value = marshal(f(*a, **kw), fields, envelope)
            value.setdefault('code', '0')
            return value, 200, {}
        return _
    return _wrapper


def marshal_with(fields, **kwargs):
    model = None
    if hasattr(fields, 'resource_fields'):
        model = fields
        fields = fields.resource_fields

    def wrapper(func):
        kwargs.setdefault('envelope', 'data')
        f = patched_marshal_with_add_code(fields, **kwargs)(func)
        f.marshal_model = model
        return f

    return wrapper


def patch_api_resource(api):
    old_add_resource = api.resource

    def add_resource(*args, **kwargs):
        def _(klass):
            return old_add_resource(*args, **kwargs)(patch_klass_swagger_attr(klass))
        return _

    api.resource = add_resource
    return api


def patch_klass_swagger_attr(klass):
    for method in ('get', 'post', 'put', 'delete', 'patch'):
        handle = getattr(klass, method, None)
        if not handle:
            continue
        swagger_attr = getattr(handle, '__swagger_attr', None)
        if swagger_attr is None:
            swagger_attr = {}
            handle.__dict__['__swagger_attr'] = swagger_attr
        patch_swagger_attr(swagger_attr, method, handle)
    return klass


def patch_swagger_attr(attr, method, handle):
    attr.setdefault('responseMessages', [])

    if getattr(handle, 'marshal_model', None):
        attr.setdefault('responseClass', handle.marshal_model)

    parameters = attr.setdefault('parameters', [])
    if not any(p['name'] == 'client_id' for p in parameters):
        parameters.insert(0, create_parameter('client_id', required=True))

    required = getattr(handle, 'login_required', False)
    parameters.append(create_parameter('Authorization', 'auth', required=required, pt='header'))

    for p in parameters:
        if 'pt' in p:
            p['paramType'] = p['pt']
        if method == 'get':
            p.setdefault('paramType', 'query')
        else:
            p.setdefault('paramType', 'form')


def create_parameter(name, description='', **kwargs):
    if description == '':
        description = name
    kwargs.setdefault('description', description)
    kwargs.setdefault('required', False)
    kwargs.setdefault('allowMultiple', False)
    kwargs.setdefault('dataType', kwargs.pop('dt', 'string'))
    kwargs['name'] = name
    return kwargs


class Resource(OriginResource):
    decorators = [oauth2_provider.require_oauth()]

    def __init__(self, *args, **kwargs):
        OriginResource.__init__(self, *args, **kwargs)
        self.parsers = {}

    def parse_args(self):
        method = request.environ['REQUEST_METHOD'].lower()
        if method not in self.parsers:
            parser = self._create_parser(getattr(self, method))
            self.parsers[method] = parser
        try:
            args = self.parsers[method].parse_args()
        except exc.InvalidArgumentError as e:
            applog('apiargumenterror', 'e=%s resource=%s method=%s', e, self.__class__.__name__, method)
            raise
        return args

    def _create_parser(self, handle):
        parameters = handle.__dict__['__swagger_attr']['parameters']
        parser = reqparse.RequestParser(argument_class=CustomArgument)
        for p in parameters:
            dt = p['dataType']
            type = p.get('type', {
                'integer': int,
                'string': lambda x: x,
                'float': float,
                'file': FileStorage,
            }.get(dt))
            if not type:
                continue
            pt = p['paramType']
            if pt == 'header':
                location = ('headers',)
            elif pt == 'body':
                location = ('json', 'files')
            else:
                location = ('values',)
            choices = p.get('choices', ())
            default = p.get('default')
            parser.add_argument(p['name'], type=type, required=p['required'], location=location, choices=choices, default=default)
        return parser


class CustomArgument(reqparse.Argument):

    def handle_validation_error(self, error, bundle_error):
        et, e, tb = sys.exc_info()
        if current_app.debug and e:
            raise et, e, tb
        if str(error).startswith('Missing required parameter'):
            # pylint: disable=E
            error = 'Missing required parameter: %s' % self.name
            # pylint: enable=E
        raise exc.InvalidArgumentError(error)  # TODO: miss what argument


def require_client():
    if 'spec' in request.url:
        return
    client_id = request.values.get('client_id')
    if client_id not in (current_app.config['OFFICIAL_CLIENT_ID'], current_app.config['OFFICIAL_IOS_CLIENT_ID'], current_app.config['OFFICIAL_ANDROID_CLIENT_ID']):
        abort(401, code=7000, description=u'invalid client_id')


def set_lat_lng_city():
    try:
        request.lat = float(request.values.get('lat', 0))
    except:
        request.lat = 0
    try:
        request.lng = float(request.values.get('lng', 0))
    except:
        request.lng = 0
    loc = request.values.get('loc')
    request.city = None
    if loc:
        request.city = locations.search_city(loc)
    if not request.city and request.lat and request.lng:
        request.city = locations.guess_by_lat_lng((request.lat, request.lng))


def set_user_recent_loc():
    if not request.user:
        return
    if not (request.lat and request.lng):
        return
    set_user_recent_lat_lng(request.user.id, request.lat, request.lng)


before_request_funcs = [require_client, set_lat_lng_city]
after_request_funcs = []


def patch_string_output():
    from flask_restful import fields
    if hasattr(fields.String, 'origin_output'):
        return
    fields.String.origin_output = fields.String.output

    def output(self, key, obj):
        v = fields.String.origin_output(self, key, obj)
        if v is None:
            v = u''
        return v

    fields.String.output = output


def trim_authoritization_header(f):
    @functools.wraps(f)
    def _(*args, **kwargs):
        request.headers.environ.pop('Authorization', None)
        return f(*args, **kwargs)
    return _

DONE = {
    'code': '0',
}


def listofmodel(model, name=None):
    name = name or '%ssModel' % model.__name__[:-len('Model')]
    _dict = dict(resource_fields={
        'list': fields.List(fields.Nested(model.resource_fields)),
    })
    klass = type(name, (object,), _dict)
    klass = swagger.nested(list=model.__name__)(klass)
    klass = swagger.model(klass)
    return klass


def listofobject(objects):
    return {
        'list': objects,
    }
