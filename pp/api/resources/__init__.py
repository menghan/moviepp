def load_sub_modules():
    import os
    import pkgutil
    import importlib
    for _, name, _ in pkgutil.iter_modules([os.path.dirname(os.path.abspath(__file__))], prefix='pp.api.resources.'):
        importlib.import_module(name)
