from datetime import datetime

from flask import request
from flask_restful import fields

from pp.model.user.loc import user_distance
from pp.lib.loc import distance_summary


class JoinField(fields.Raw):

    def output(self, key, obj):
        user = request.user
        if not user:
            return '0'
        return '1' if obj.is_joined_by(user.id) else '0'


class BooleanStringField(fields.Raw):

    def format(self, value):
        return '1' if bool(value) else '0'


class DateField(fields.Raw):

    def output(self, key, obj):
        return str(datetime.now().date())


class LocationField(fields.String):
    def format(self, value):
        if not value:
            return u''
        from pp.model.location.models import locations
        l = locations.get(value)
        return l.name if l else u''


class DistField(fields.Raw):

    def output(self, key, obj):
        cinema = obj
        return cinema.dist_desc()


# FIXME: rename
class StaticField(fields.String):
    def format(self, value):
        if value and not value.startswith('http'):
            value = '%s/%s/%s' % (request.url_root.rstrip('/'), 'static', value.lstrip('/'))
        return value


class NameField(fields.String):
    def format(self, value):
        return value.name


class UserDistField(fields.String):

    def output(self, key, obj):
        # may runned in mq workers
        user1 = getattr(request, 'user')
        if not user1 or not obj:
            return None
        dist = user_distance(user1, obj)
        if dist:
            return distance_summary(dist)
