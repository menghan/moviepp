# coding=utf-8

import sys
from flask import current_app, request
from flask_restful import fields
from flask_restful_swagger import swagger
from flask_login import current_user

from pp.api.utils import marshal_with, create_parameter, Resource, login_required, listofmodel
from pp.api.app import api
from pp.api.resources.fields import JoinField
from pp.model.chat.models import chatrooms, messages, lastmessages, MessageType, privchats, new_priv_message, new_message
from pp.model.chat.models import ChatRoomModel, ChatRoomFullModel, MessageModel, BuddyModel, ChatRoomUpdateModel, PrivChatModel, ChatUnitModel
from pp.model.user.notification import notifications


@swagger.model
@swagger.nested(
    list=ChatRoomUpdateModel.__name__,
)
class ChatRoomsUpdateModel(object):
    resource_fields = {
        'at': fields.String,  # notification by @me
        'list': fields.List(fields.Nested(ChatRoomUpdateModel.resource_fields)),
    }


@api.resource('/chatrooms_update')
class ChatRoomUpdates(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('chatroom_ids', required=True),
        ],
    )
    @marshal_with(ChatRoomsUpdateModel)
    def get(self):
        '''get chatrooms' latest messages according to chatroom ids(joined by `,`)'''
        args = self.parse_args()
        chatroom_ids = map(int, filter(None, args.chatroom_ids.split(',')))
        at = 0
        if request.user:
            noti = notifications.get(request.user.id)
            if noti and noti.has_at_me:
                at = 1
        return {
            'at': at,
            'list': [lm.to_dict() for lm in lastmessages.gets(chatroom_ids) if lm],
        }


@api.resource('/mychatrooms_update')
class MyChatRoomUpdates(Resource):
    @login_required
    @marshal_with(ChatRoomsUpdateModel)
    def get(self):
        '''get my chatrooms' latest messages'''
        chatroom_ids = chatrooms.gets_ids_by_user(request.user.id)
        at = 0
        if request.user:
            noti = notifications.get(request.user.id)
            if noti and noti.has_at_me:
                at = 1
        return {
            'at': at,
            'list': lastmessages.gets(chatroom_ids),
        }


@api.resource('/chatrooms/<int:chatroom_id>')
class ChatRoom(Resource):
    @marshal_with(ChatRoomFullModel)
    def get(self, chatroom_id):
        '''Get chatroom detail'''
        chatroom = chatrooms.get(chatroom_id)
        chatroom.remove_at_me()
        return chatroom


@api.resource('/chatrooms')
class ChatRooms(Resource):
    @login_required
    @marshal_with(listofmodel(ChatRoomModel))
    def get(self):
        '''Get my chatrooms'''
        noti = notifications.get(request.user.id)
        if noti and noti.has_at_me:
            noti.orm.update(noti, has_at_me=0)
        return {
            'list': chatrooms.gets_by_user(request.user.id),
        }


@api.resource('/chatrooms/<int:chatroom_id>/history')
class ChatRoomHistory(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('max_id', required=False, dt='integer'),
            create_parameter('limit', dt='integer', default=20),
        ],
    )
    @marshal_with(listofmodel(MessageModel, 'ChatRoomHistoryModel'))
    def get(self, chatroom_id):
        '''Get chatroom history'''
        args = self.parse_args()
        args.max_id = args.max_id or sys.maxint
        history = messages.history(MessageType.normal, owner_id=chatroom_id,
                                   other_id=None,  # other_id
                                   chatroom_id=chatroom_id,
                                   max_id=args.max_id, limit=args.limit)
        return {
            'list': history,
        }


@api.resource('/chatrooms/<int:chatroom_id>/buddies')
class ChatRoomBuddies(Resource):
    @marshal_with(listofmodel(BuddyModel))
    def get(self, chatroom_id):
        '''Get chatroom buddies'''
        chatroom = chatrooms.get(chatroom_id)
        buddies = chatroom.buddies
        return {
            'list': buddies,
        }


@api.resource('/chatrooms/<int:chatroom_id>/send_message')
class ChatRoomSendMessage(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('payload'),
            create_parameter('random_id', dt='integer'),
        ],
    )
    @login_required
    @marshal_with(MessageModel)
    def post(self, chatroom_id):
        '''Create chatroom message'''
        args = self.parse_args()
        message = new_message(request.user.id, chatroom_id, args.payload, MessageType.normal, args.random_id)
        return message


@api.resource('/privchats')
class PrivChats(Resource):
    @login_required
    @marshal_with(listofmodel(PrivChatModel))
    def get(self):
        '''Get my privchats'''
        noti = notifications.get(request.user.id)
        if noti and noti.has_at_me:
            noti.orm.update(noti, has_at_me=0)
        return {
            'list': privchats.gets_by_user(request.user.id),
        }


@api.resource('/privchats/<int:privchat_id>')
class PrivChat(Resource):
    @login_required
    @marshal_with(PrivChatModel)
    def get(self, privchat_id):
        '''Get privchat detail'''
        privchat = privchats.get(request.user.id, privchat_id)
        privchat.remove_at_me()
        return privchat


@api.resource('/privchats/<int:privchat_id>/history')
class PrivChatHistory(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('max_id', required=False, dt='integer'),
            create_parameter('limit', dt='integer', default=20),
        ],
    )
    @login_required
    @marshal_with(listofmodel(MessageModel, 'PrivChatHistoryModel'))
    def get(self, privchat_id):
        '''Get privchat history'''
        args = self.parse_args()
        args.max_id = args.max_id or sys.maxint
        history = messages.history(type=MessageType.priv, owner_id=request.user.id, other_id=privchat_id, chatroom_id=None, max_id=args.max_id, limit=args.limit)
        return {
            'list': history,
        }


@api.resource('/privchats/<int:privchat_id>/send_message')
class PrivChatSendMessage(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('payload'),
            create_parameter('random_id', dt='integer'),
        ],
    )
    @login_required
    @marshal_with(MessageModel)
    def post(self, privchat_id):
        '''Create message'''
        args = self.parse_args()
        message = new_priv_message(request.user.id, privchat_id, args.payload, args.random_id)
        return message


@api.resource('/chats')
class Chats(Resource):
    @login_required
    @marshal_with(listofmodel(ChatUnitModel))
    def get(self):
        '''Get all my chats'''
        noti = notifications.get(request.user.id)
        if noti and noti.has_at_me:
            noti.orm.update(noti, has_at_me=0)
        privs = privchats.gets_by_user(request.user.id)
        rooms = chatrooms.gets_by_user(request.user.id)
        chats = privs + rooms
        chats.sort(key=lambda c: c.updated_at, reverse=True)
        return {
            'list': chats,
        }
