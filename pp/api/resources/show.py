# coding=utf-8

from datetime import datetime, timedelta
import itertools
from collections import defaultdict

from flask import current_app, request
from flask_restful import fields
from flask_restful_swagger import swagger
from flask_login import current_user

import pp.lib.exc as exc
from pp.lib.perf import lazy_to_batch
from pp.lib.logger import applog
from pp.api.app import api
from pp.api.utils import login_required, marshal_with, create_parameter, Resource, DONE, listofmodel, listofobject, set_user_recent_loc
from pp.model.location.models import locations
from pp.model.user.models import users, UserModel
from pp.model.show.models import cinemas, movies, shows, favcinemas, tickets, ALL_RETAILERS
from pp.model.show.models import CinemaModel, MovieModel, MovieFullModel, TicketModel, ShowModel, RetailerModel
from pp.model.chat.models import chatrooms, messages, lastmessages, ChatRoomModel

CinemasModel = listofmodel(CinemaModel)


@swagger.model
@swagger.nested(
    cinemas=CinemaModel.__name__,
    movies=MovieModel.__name__,
    retailers=RetailerModel.__name__,
)
class CinemasMoviesRetailersModel(object):
    resource_fields = {
        'cinemas': fields.List(fields.Nested(CinemaModel.resource_fields)),
        'movies': fields.List(fields.Nested(MovieModel.resource_fields)),
        'retailers': fields.List(fields.Nested(RetailerModel.resource_fields)),
    }


@swagger.model
@swagger.nested(
    chats=ChatRoomModel.__name__,
    movie=MovieModel.__name__,
)
class MovieChatRoomModel(object):
    resource_fields = {
        'movie': fields.Nested(MovieModel.resource_fields),
        'chats': fields.List(fields.Nested(ChatRoomModel.resource_fields)),
        'nchats': fields.String,
    }


@swagger.model
@swagger.nested(
    movies=MovieChatRoomModel.__name__,
)
class ScheduleModel(object):
    resource_fields = {
        'date': fields.String,
        'movies': fields.List(fields.Nested(MovieChatRoomModel.resource_fields)),
        'fav': fields.String,
        'addr': fields.String,
        'chats_count': fields.String,
    }


@swagger.model
@swagger.nested(
    cinema=CinemaModel.__name__,
    schedule=ScheduleModel.__name__,
)
class CinemaChatModel(object):
    resource_fields = {
        'cinema': fields.Nested(CinemaModel.resource_fields),
        'schedule': fields.Nested(ScheduleModel.resource_fields),
    }


@swagger.model
@swagger.nested(
    cinemachatlist=CinemaChatModel.__name__,
)
class FirstScreenModel(object):
    resource_fields = {
        'city': fields.String,
        'cinemachatlist': fields.List(fields.Nested(CinemaChatModel.resource_fields)),
    }


@swagger.model
class CinemaMovieShowModel(object):
    resource_fields = {
        'time': fields.String,
        'hall': fields.String,
    }


@api.resource('/cinemasmoviesretailers')
class CinemasMoviesRetailers(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('q', 'search keyword'),
            create_parameter('lat'),
            create_parameter('lng'),
            create_parameter('loc'),
        ],
    )
    @marshal_with(CinemasMoviesRetailersModel)
    def get(self):
        '''Return Cinemas, Movies, Retailers in one api'''
        args = self.parse_args()
        city = request.city or locations.get_beijing()
        cs = cinemas.gets_by(city, q=args.q, lat=args.lat, lng=args.lng)
        if request.lat and request.lng:
            cs.sort(key=lambda c: c.dist())
        return {
            'cinemas': cs,
            'movies': movies.onshow_movies(),
            'retailers': ALL_RETAILERS,
        }


@api.resource('/movies/<int:movie_id>')
class Movie(Resource):
    @marshal_with(MovieFullModel)
    def get(self, movie_id):
        '''get movie details'''
        movie = movies.get(movie_id)
        if not movie:
            raise exc.MovieNotFoundError
        return movie


@api.resource('/movies/<int:movie_id>/watched_users')
class MovieWatchedUsers(Resource):
    @marshal_with(listofmodel(UserModel))
    def get(self, movie_id):
        '''get movie watched users'''
        user_ids = movies.get_watched_user_ids(movie_id)
        return listofobject(users.gets(user_ids))


@api.resource('/cinemas')
class Cinemas(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('q', 'search keyword'),
            create_parameter('lat'),
            create_parameter('lng'),
            create_parameter('loc'),
        ],
    )
    @marshal_with(CinemasModel)
    def get(self):
        '''Search cinemas'''
        args = self.parse_args()
        city = request.city or locations.get_beijing()
        cs = cinemas.gets_by(city, q=args.q, lat=args.lat, lng=args.lng)
        if request.lat and request.lng:
            cs.sort(key=lambda c: c.dist())
        return {
            'list': cs,
        }


@api.resource('/cinemas/<int:cinema_id>/visited_users')
class CinemaVisitedUsers(Resource):
    @marshal_with(listofmodel(UserModel))
    def get(self, cinema_id):
        '''get cinema visited users'''
        user_ids = cinemas.get_visited_user_ids(cinema_id)
        return listofobject(users.gets(user_ids))


@api.resource('/favcinemas')
class FavCinemas(Resource):
    @login_required
    @marshal_with(CinemasModel)
    def get(self):
        '''my fav cinemas'''
        return {
            'list': favcinemas.get_cinemas_by_user(request.user.id),
        }

    @swagger.operation(
        parameters=[
            create_parameter('cinema_id', required=True, dt='integer'),
        ],
    )
    @login_required
    @marshal_with(CinemasModel)
    def post(self):
        '''save this cinema to my fav list'''
        args = self.parse_args()
        if not cinemas.get(args.cinema_id):
            # raise some error
            pass
        favcinemas.create_ignore(
            user_id=request.user.id,
            cinema_id=args.cinema_id,
        )
        return {
            'list': favcinemas.get_cinemas_by_user(request.user.id),
        }


@api.resource('/favcinemas/<int:cinema_id>/delete')
class FavCinema(Resource):
    @login_required
    @marshal_with(CinemasModel)
    def post(self, cinema_id):
        '''delete this cinema from my fav list'''
        o = favcinemas.first(user_id=request.user.id, cinema_id=cinema_id)
        if o:
            applog('ormdelete', 'delete favcinema id=%s vars=%r', o.id, vars(o))
            favcinemas.delete(o)
        return {
            'list': favcinemas.get_cinemas_by_user(request.user.id),
        }


@api.resource('/onshowmovies')
class OnshowMovies(Resource):
    @marshal_with(listofmodel(MovieModel))
    def get(self):
        '''Get onshow movies'''
        return {
            'list': movies.onshow_movies(),
        }


@api.resource('/retailers')
class Retailers(Resource):
    @marshal_with(listofmodel(RetailerModel))
    def get(self):
        '''Retailer list'''
        return {
            'list': ALL_RETAILERS,
        }


@api.resource('/shows/cinema/<int:cinema_id>/<string:date>')
class Shows(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('digest', required=False),
        ],
    )
    @marshal_with(ScheduleModel)
    def get(self, cinema_id, date):
        '''fetch cinema schedules/chatrooms on some day'''
        args = self.parse_args()
        if args.digest:
            filter_func = return_one_show_after_7pm_if_can
        else:
            filter_func = None
        cinema = cinemas.get(cinema_id)
        try:
            date = datetime.strptime(date, '%Y-%m-%d').date()
        except ValueError:
            date = datetime.now().date()
        return get_cinemas_schedule([cinema], date=date, filter_func=filter_func)[0]


@api.resource('/shows/cinema/<int:cinema_id>/movie/<int:movie_id>')
class CinemaMovieShows(Resource):
    @marshal_with(listofmodel(CinemaMovieShowModel))
    def get(self, cinema_id, movie_id):
        '''fetch suggest time/halls for a cinema and a movie'''
        today = datetime.now().date()
        objs = shows.findall(cinema_id=cinema_id, movie_id=movie_id)
        objs.sort(key=lambda show: show.time)
        objs = [obj for obj in objs if obj.date >= today - timedelta(days=1)]
        return {
            'list': objs,
        }


@api.resource('/tickets/<int:ticket_id>')
class Ticket(Resource):
    @login_required
    @marshal_with(TicketModel)
    def get(self, ticket_id):
        '''Get ticket info'''
        ticket = tickets.get(ticket_id)
        return ticket

    @swagger.operation(
        parameters=[
            create_parameter(name, dt=data_type)
            for (name, data_type) in [
                    ('hall', 'string'),
                    ('price', 'integer'),
                    ('fee', 'integer'),
                    ('seats', 'string'),
                    ('retailer_id', 'integer'),
            ]
        ],
    )
    @login_required
    @marshal_with(TicketModel)
    def post(self, ticket_id):
        '''Update ticket info'''
        args = self.parse_args()
        ticket = tickets.get(ticket_id)
        if not ticket:
            raise exc.TicketNotFoundError
        if ticket.user_id != request.user.id:
            raise exc.TicketNotOwnedError
        ticket.update_detail(**args)
        ticket = tickets.get(ticket_id)
        return ticket


@api.resource('/chatrooms/<int:chatroom_id>/ticket')
class ChatRoomTicket(Resource):
    @login_required
    @marshal_with(TicketModel)
    def get(self, chatroom_id):
        '''Get user's ticket in this chatroom'''
        show_id = chatroom_id
        ticket = tickets.first(user_id=request.user.id, show_id=show_id)
        if not ticket:
            raise exc.TicketNotFoundError
        return ticket


@api.resource('/tickets/<int:ticket_id>/delete')
class DeleteTicket(Resource):
    @login_required
    def post(self, ticket_id):
        '''Delete ticket from my tickets'''
        ticket = tickets.get(ticket_id)
        if not ticket:
            raise exc.TicketNotFoundError
        if ticket.user_id != request.user.id:
            raise exc.TicketNotOwnedError
        applog('ormdelete', 'delete ticket id=%s vars=%r', ticket.id, vars(ticket))
        tickets.delete(ticket)
        return DONE


@api.resource('/tickets')
class Tickets(Resource):
    @login_required
    @marshal_with(listofmodel(TicketModel))
    def get(self):
        '''Get my tickets'''
        args = self.parse_args()
        ticket_list = tickets.findall(user_id=request.user.id)
        ticket_list.reverse()
        return {
            'list': ticket_list,
        }

    @swagger.operation(
        parameters=[
            create_parameter(name, dt=data_type, required=required)
            for (name, data_type, required) in [
                    ('cinema_id', 'integer', False),
                    ('movie_id', 'integer', False),
                    ('hall', 'string', False),
                    ('price', 'integer', False),
                    ('fee', 'integer', False),
                    ('seats', 'string', False),
                    ('date', 'string', False),
                    ('time', 'string', False),
                    ('retailer_id', 'integer', False),
                    ('chatroom_id', 'integer', True),
            ]
        ],
    )
    @login_required
    @marshal_with(TicketModel)
    def post(self):
        '''Add ticket to my tickets'''
        args = self.parse_args()
        ticket = tickets.add_by_show_id(request.user.id, args.chatroom_id, **args)
        if not ticket:
            raise exc.CantMatchShowError
        return ticket


@api.resource('/firstscreen')
class FirstScreen(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('lat', dt='float'),
            create_parameter('lng', dt='float'),
            create_parameter('loc'),
            create_parameter('date'),
            create_parameter('thin'),
        ],
    )
    @marshal_with(FirstScreenModel)
    def get(self):
        '''Get first screen info'''
        set_user_recent_loc()
        args = self.parse_args()
        city = request.city or locations.get_beijing()
        cinema_list = cinemas.gets_by(city, lat=args.lat, lng=args.lng)
        date = datetime.strptime(args.date, '%Y-%m-%d').date() if args.date else datetime.now().date()
        cclist = []

        if args.thin:
            cclist = [{'cinema': cinema} for cinema in cinema_list]
            # filter duplicated
            duplicated = defaultdict(list)
            for v in cclist:
                duplicated[v['cinema'].name].append(v['cinema'])
            duplicated = dict((k, v) for k, v in duplicated.iteritems() if len(v) > 1)
            keep_cinema_ids = set()
            for dup_cinemas in duplicated.values():
                schedules = get_cinemas_schedule(dup_cinemas, date)
                max_cinema_id = None
                max_schedule_count = 0
                for cinema, schedule in zip(dup_cinemas, schedules):
                    if len(schedule['movies']) >= max_schedule_count:
                        max_cinema_id = cinema.id
                        max_schedule_count = len(schedule['movies']) + 1
                keep_cinema_ids.add(max_cinema_id)
            cclist = [v for v in cclist if v['cinema'].name not in duplicated or v['cinema'].id in keep_cinema_ids]
        else:
            schedules = get_cinemas_schedule(cinema_list, date, return_one_show_after_7pm_if_can)
            for i, cinema in enumerate(cinema_list):
                schedule = schedules[i]
                if not schedule['chats_count']:
                    continue
                cclist.append({
                    'cinema': cinema,
                    'schedule': schedule,
                })
        if request.lat and request.lng:
            cclist.sort(key=lambda c: c['cinema'].dist())
        else:
            if cclist and 'schedule' in cclist[0]:
                cclist.sort(key=lambda c: c['schedule']['chats_count'], reverse=True)
        return {
            'city': city.name,
            'cinemachatlist': cclist,
        }


def return_one_show_after_7pm_if_can(showlist):
    after_7pms = filter(lambda show: show.time.hour >= 19, showlist)
    if after_7pms:
        return [after_7pms[0]]
    return [showlist[0]]


@api.resource('/shows/<int:show_id>')
class Show(Resource):
    @marshal_with(ShowModel)
    def get(self, show_id):
        '''return show info'''
        show = shows.get(show_id)
        if not show:
            raise exc.ShowNotFoundError
        return show


def get_cinemas_schedule(cinemas, date, filter_func=None):
    schedules = []
    # 不展示90天之前的影讯
    if date < (datetime.today() - timedelta(days=90)).date():
        return schedules
    for cinema in cinemas:
        schedule_groupby_movies = get_cinema_schedule(cinema, date, filter_func)
        schedule = {
            'date': str(date),  # FIXME: return nearest schedule day
            'movies': schedule_groupby_movies,
            'fav': cinema.fav,
            'addr': cinema.addr,
        }
        schedules.append(schedule)

    # to make it lazy while iterating
    for schedule in schedules:
        schedule['chats_count'] = sum([ms['nchats'] for ms in schedule['movies']])
    return schedules


def warm_by_cinema_date(akws):
    cinemas = []
    dates = set()
    for a, kw in akws:
        cinema, date, filter_func = a
        cinemas.append(cinema)
        dates.add(date)
    assert len(dates) == 1
    date = list(dates)[0]

    show_list = sum([shows.findall(cinema_id=cinema.id, date=date) for cinema in cinemas], [])
    movie_ids = list(set(show.movie_id for show in show_list))
    movies.gets(movie_ids)

    lms = filter(None, lastmessages.gets([show.id for show in show_list]))
    message_ids = [lm.message_id for lm in lms]
    message_list = messages.gets(message_ids)
    user_ids = list(set(message.user_id for message in message_list))
    users.gets(user_ids)

    if request.user:
        tickets.findall(user_id=request.user.id)


# @lazy_to_batch(warm_by_cinema_date)
def get_cinema_schedule(cinema, date, filter_func):
    schedule_groupby_movies = []
    show_list = shows.findall(cinema_id=cinema.id, date=date)
    show_list.sort(key=lambda show: show.movie_id)
    movie_groups = [
        (movie_id, list(sorted(show_sub_list, key=lambda show: show.time)))
        for movie_id, show_sub_list in itertools.groupby(show_list, lambda show: show.movie_id)
    ]
    movie_groups.sort(key=lambda (movie_id, show_sub_list): len(show_sub_list), reverse=True)
    for movie_id, show_sub_list in movie_groups:
        chats = []
        nchats = len(show_sub_list)
        show_sub_list.sort(key=lambda show: show.time)
        if filter_func is not None:
            show_sub_list = filter_func(show_sub_list)
        for show in show_sub_list:
            chatroom = chatrooms(show.id)
            chats.append(chatroom)
        schedule_groupby_movies.append({
            'movie': movies.get(movie_id),
            'chats': chats,
            'nchats': nchats,
        })
    return schedule_groupby_movies
