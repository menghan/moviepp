# coding=utf-8

import json
import functools

from flask import request, g
from flask_restful import fields
from flask_restful_swagger import swagger

import pp.lib.exc as exc
from pp.lib.logger import applog
from pp.api.app import api
from pp.api.utils import login_required, marshal, marshal_with, create_parameter, Resource, trim_authoritization_header, DONE
from pp.api.oauth2 import oauth2_provider
from pp.model.oauth2.models import load_token
from pp.model.sms.models import send
from pp.model.user.models import accounts, phone_register, phone_reset_password, phone_change_password
from pp.model.user.models import ProfileModel
from pp.model.user.verifycode import VerifyCode


def returnprofile(f):
    @functools.wraps(f)
    def _(*a, **kw):
        v = f(*a, **kw)
        profile = marshal(g.loaded_user, ProfileModel.resource_fields, 'data')
        d = json.loads(v.get_data())
        d.update(profile)
        v.set_data(json.dumps(d))
        return v
    return _


@api.resource('/accounts/access_token')
class AccessToken(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('client_secret', 'client\'s secret', required=True),
            create_parameter('grant_type', 'be "password" or "refresh_token"', required=True),
            create_parameter('username', 'username, phone number for here, "weixin" for weixin login'),
            create_parameter('password', 'password or weixin grant code'),
            create_parameter('refresh_token'),
            create_parameter('weixin_connect', 'if weixin, send "1", else ""'),
        ],
    )
    @trim_authoritization_header
    @returnprofile
    @oauth2_provider.token_handler
    def post(self):
        '''get access_token
        For user login, grant_type be "password" and send username, password.
        For refresh access_token, grant_type be "refresh_token" and send refresh_token.'''
        args = self.parse_args()
        applog('accounts', '/access_token: %s', args)
        if args.grant_type == 'refresh_token':
            refresh_token = load_token(refresh_token=args.refresh_token)
            if refresh_token:
                applog('accounts', '/access_token: refresh_token: user_id=%s', refresh_token.user_id)
        return DONE


@api.resource('/accounts/me')
class Me(Resource):
    @login_required
    @marshal_with(ProfileModel)
    def get(self):
        '''get user info of myself'''
        return request.user


@api.resource('/accounts/resetpassword')
class ResetPassword(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('username', 'username, phone number for here', required=True),
            create_parameter('password', required=True),
            create_parameter('verify_code', required=True),
        ],
    )
    def post(self):
        '''reset user's password'''
        args = self.parse_args()
        phone_reset_password(args.username, args.password, args.verify_code)
        applog('accounts', '/resetpassword: username=%s', args.username)
        return DONE


@api.resource('/accounts/changepassword')
class ChangePassword(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('username', 'username, phone number for here', required=True),
            create_parameter('password', required=True),
            create_parameter('new_password', required=True),
        ],
    )
    def post(self):
        '''change user's password'''
        args = self.parse_args()
        phone_change_password(args.username, args.password, args.new_password)
        applog('accounts', '/changepassword: username=%s', args.username)
        return DONE


@api.resource('/accounts/register')
class Register(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('username', 'username, phone number for here', required=True),
            create_parameter('password', required=True),
            create_parameter('verify_code', required=True),
        ],
    )
    def post(self):
        '''register'''
        args = self.parse_args()
        phone_register(args.username, args.password, args.verify_code)
        applog('accounts', '/register: username=%s', args.username)
        return DONE


@api.resource('/accounts/sendverifycode')
class SendVerifyCode(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('phone', required=True),
            create_parameter('type', required=True, choices=('register', 'resetpassword')),
        ],
    )
    def post(self):
        '''send verify code'''
        args = self.parse_args()
        if not args.phone.isdigit() or len(args.phone) != 11:
            raise exc.InvalidPhoneError
        if args.type == 'resetpassword' and not accounts.first(phone=args.phone):
            raise exc.UserNotFoundError
        code = VerifyCode.generate(args.phone)
        send(args.phone, '您的验证码是%s' % code)
        applog('accounts', '/sendverifycode: phone=%s', args.phone)
        return DONE
