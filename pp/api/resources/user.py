# coding=utf-8

from flask import current_app, request, abort
from flask_restful import fields
from flask_restful_swagger import swagger
from flask_login import current_user

import pp.lib.exc as exc
from pp.api.app import api
from pp.api.utils import login_required, marshal_with, create_parameter, Resource, DONE, listofmodel, listofobject
from pp.api.resources.fields import LocationField, StaticField
from pp.lib.sqlstore import db
from pp.model.upload.models import uploads
from pp.model.location.models import locations
from pp.model.user.models import users, GenderType, UserModel, ProfileModel, AvatarModel, StatModel
from pp.model.user.blacklist import blacklists
from pp.model.user.settings import bit_settings, setting_map, SettingsModel
from pp.model.user.follow import get_following_user_ids, get_followed_user_ids, follow, unfollow
from pp.model.show.models import tickets, favcinemas
from pp.model.show.models import CinemaModel, MovieModel, user_visited_cinemas, user_watched_movies


@api.resource('/users/<int:user_id>')
class User(Resource):
    @marshal_with(UserModel)
    def get(self, user_id):
        '''Get user details'''
        u = users.get(user_id)
        if not u:
            raise exc.AccountNotExistError
        return u


@api.resource('/profile')
class Profile(Resource):

    @swagger.operation(
        parameters=[
            create_parameter('nickname'),
            create_parameter('favloc'),
            create_parameter('gender'),
        ],
    )
    @login_required
    @marshal_with(ProfileModel)
    def post(self):
        '''update my profile'''
        args = self.parse_args()
        u = users.get(request.user.id)
        u.nickname = args.nickname.replace(u' ', u'')
        city = locations.search_city(args.favloc)
        if not city:
            raise exc.CityNotSetOrNotFoundError(args.favloc)
        u.favloc_id = city.id
        if args.gender == 'U':
            u.gender = GenderType.unset
        elif args.gender == 'M':
            u.gender = GenderType.male
        elif args.gender == 'F':
            u.gender = GenderType.female
        u.orm.save(u)
        return u


@api.resource('/stat')
class Stat(Resource):

    @login_required
    @marshal_with(StatModel)
    def get(self):
        '''get my stats'''
        return {
            'fav_cinema_count': favcinemas.get_count_by_user(request.user.id),
            'ticket_count': tickets.get_count_by_user(request.user.id),
        }


@api.resource('/blacklist')
class Blacklist(Resource):

    # @login_required
    # def get(self):
    #     '''get my blacklist'''
    #     return {
    #         'list': [b.target_id for b in blacklists.findall(user_id=request.user.id)],
    #     }

    @swagger.operation(
        parameters=[
            create_parameter('user_id', required=True, dt='integer'),
            create_parameter('action', required=True, choices=('add', 'remove')),
        ],
    )
    @login_required
    def post(self):
        '''add/remove someone into/from blacklist'''
        args = self.parse_args()
        if args.action == 'add':
            blacklists.create_ignore(user_id=request.user.id, target_id=args.user_id)
        else:
            o = blacklists.first(user_id=request.user.id, target_id=args.user_id)
            if o:
                blacklists.delete(o)
        return DONE


@api.resource('/settings')
class Settings(Resource):
    @login_required
    @marshal_with(SettingsModel)
    def get(self):
        '''Get my settings'''
        user_id = current_user.id
        return bit_settings.get_user_settings(user_id)

    @swagger.operation(
        parameters=[
            create_parameter(k, required=True, choices=('1', '0'))
            for k in setting_map
        ],
    )
    @login_required
    @marshal_with(SettingsModel)
    def post(self):
        '''Update my settings'''
        args = self.parse_args()
        user_id = current_user.id
        bit_settings.set_user_settings(user_id, **args)
        return bit_settings.get_user_settings(user_id)


@api.resource('/avatar')
class Avatar(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('avatar', required=True, pt='body', dt='file'),
        ],
    )
    @login_required
    @marshal_with(AvatarModel)
    def post(self):
        '''Upload user's avatar'''
        user = request.user
        args = self.parse_args()
        up_id = uploads.upload(args['avatar'])
        user.avatar_id = up_id
        user.orm.save(user)
        return user


@api.resource('/users/<int:user_id>/following')
class Following(Resource):

    @login_required
    @marshal_with(listofmodel(UserModel))
    def get(self, user_id):
        if user_id != request.user.id:
            abort(403)
        us = users.gets(get_following_user_ids(user_id))
        return listofobject(us)


@api.resource('/users/<int:user_id>/followed')
class Followed(Resource):

    @login_required
    @marshal_with(listofmodel(UserModel))
    def get(self, user_id):
        if user_id != request.user.id:
            abort(403)
        us = users.gets(get_followed_user_ids(user_id))
        return listofobject(us)


@api.resource('/users/<int:target_id>/<string:action>')
class Followship(Resource):

    @login_required
    def post(self, target_id, action):
        target_user = users.get(target_id)
        if not target_user:
            return DONE
        if action == 'follow':
            follow(request.user.id, target_id)
        elif action == 'unfollow':
            unfollow(request.user.id, target_id)
        return DONE


@api.resource('/users/<int:user_id>/visited_cinemas')
class VisitedCinemas(Resource):

    @marshal_with(listofmodel(CinemaModel))
    def get(self, user_id):
        return {
            'list': user_visited_cinemas(user_id),
        }


@api.resource('/users/<int:user_id>/watched_movies')
class WatchedMovies(Resource):

    @marshal_with(listofmodel(MovieModel))
    def get(self, user_id):
        return {
            'list': user_watched_movies(user_id),
        }
