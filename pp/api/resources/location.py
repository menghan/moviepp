# coding=utf-8

import itertools
from flask import current_app, request
from flask_restful import fields
from flask_restful_swagger import swagger

from pp.api.app import api
from pp.api.utils import marshal_with, create_parameter, Resource
from pp.model.location.models import locations, LocationModel


@swagger.model
@swagger.nested(
    list=LocationModel.__name__,
)
class LocationGroupModel(object):
    resource_fields = {
        'list': fields.List(fields.Nested(LocationModel.resource_fields)),
        'prefix': fields.String,
    }


@swagger.model
@swagger.nested(
    hot=LocationModel.__name__,
    list=LocationGroupModel.__name__,
)
class LocationsModel(object):
    resource_fields = {
        'city': fields.String,
        'hot': fields.List(fields.Nested(LocationModel.resource_fields)),
        'list': fields.List(fields.Nested(LocationGroupModel.resource_fields)),
    }


@api.resource('/locations')
class Locations(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('lat'),
            create_parameter('lng'),
        ],
    )
    @marshal_with(LocationsModel)
    def get(self):
        '''get hot/all locations'''
        args = self.parse_args()
        all_cities = locations.all_cities()
        all_cities.sort(key=lambda c: c.en_name)
        group_func = lambda d: d.en_name[0].upper()
        all_cities.sort(key=group_func)
        return {
            'city': request.city.name if request.city else '',
            'hot': locations.hot_cities(),
            'list': [
                {
                    'prefix': prefix,
                    'list': list(lst),
                } for prefix, lst in itertools.groupby(all_cities, key=group_func)
            ],
        }
