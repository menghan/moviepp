# coding=utf-8

from flask import request
from flask_restful_swagger import swagger

from pp.lib.logger import applog
from pp.api.app import api
from pp.api.utils import create_parameter, Resource, DONE
from pp.model.report.models import reports


@api.resource('/report')
class Report(Resource):
    @swagger.operation(
        parameters=[
            create_parameter('content', required=True),
        ],
    )
    def post(self):
        '''Report something'''
        args = self.parse_args()
        applog('report', 'user_id=%s,content="%s"', request.user.id if request.user else '', args.content)
        reports.create(user_id=request.user.id if request.user else 0, content=args.content)
        return DONE
