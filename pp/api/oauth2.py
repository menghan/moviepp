from flask import request
from flask_oauthlib.provider import OAuth2Provider
from pp.model.oauth2.models import init_oauth2_provider

oauth2_provider = OAuth2Provider()
init_oauth2_provider(oauth2_provider)


@oauth2_provider.after_request
def _(valid, oauth):
    '''
    We want oauth2 check all api interfaces, so these functions which don't
    require an oauth2 login but accept an oauth2 login will happy.
    '''
    request.user = oauth.user
    return True, oauth
