# coding=utf-8

from datetime import datetime, timedelta

from werkzeug.utils import cached_property
from flask import current_app, g
from flask_login import current_user

from pp.lib.sqlstore import db, create_service
from pp.model.user.models import users, phone_login, weixin_login


class Client(db.Model):

    client_id = db.Column(db.String(40), primary_key=True)
    client_secret = db.Column(db.String(55), nullable=False)
    _redirect_uris = db.Column(db.Text)
    _default_scopes = db.Column(db.Text)

    def __repr__(self):
        return '%s<client_id=%d>' % (self.__class__.__name__, self.client_id)

    @cached_property
    def client_type(self):
        return 'confidential'

    @cached_property
    def redirect_uris(self):
        if self._redirect_uris:
            return self._redirect_uris.split()
        return []

    @cached_property
    def default_redirect_uri(self):
        return self.redirect_uris[0]

    @cached_property
    def default_scopes(self):
        if self._default_scopes:
            return self._default_scopes.split()
        return []


class Grant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, index=True)
    client_id = db.Column(db.String(40), index=True)
    code = db.Column(db.String(255), index=True, nullable=False)
    redirect_uri = db.Column(db.String(255))
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def user(self):
        return users.get(self.user_id)

    @cached_property
    def client(self):
        return clients.first(client_id=self.client_id)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @cached_property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []


class Token(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(40), index=True)
    user_id = db.Column(db.Integer, index=True)
    token_type = db.Column(db.String(40))  # currently only 'Bearer' is supported
    access_token = db.Column(db.String(255), unique=True)
    refresh_token = db.Column(db.String(255), unique=True)
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def user(self):
        return users.get(self.user_id)

    @cached_property
    def client(self):
        return clients.first(client_id=self.client_id)

    @cached_property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []

clients = create_service(Client)
grants = create_service(Grant)
tokens = create_service(Token)


def load_client(client_id):
    return clients.first(client_id=client_id)


def load_grant(client_id, code):
    return grants.first(client_id=client_id, code=code)


def save_grant(client_id, code, request, *args, **kwargs):
    # decide the expires time yourself
    expires = datetime.utcnow() + timedelta(seconds=100)
    grant = grants.create(
        client_id=client_id,
        code=code['code'],
        redirect_uri=request.redirect_uri,
        _scopes=' '.join(request.scopes),
        user=current_user,
        expires=expires
    )
    return grant


def load_token(access_token=None, refresh_token=None):
    if access_token:
        return tokens.first(access_token=access_token)
    elif refresh_token:
        return tokens.first(refresh_token=refresh_token)


def save_token(token, request, *args, **kwargs):
    toks = tokens.findall(
        user_id=request.user.id
    )
    # make sure that there is only one token connected to a user
    for t in toks:
        tokens.delete(t)

    expires_in = token['expires_in']
    expires = datetime.utcnow() + timedelta(seconds=expires_in)

    tok = tokens.create(
        access_token=token['access_token'],
        refresh_token=token['refresh_token'],
        token_type=token['token_type'],
        _scopes=token['scope'],
        expires=expires,
        client_id=request.client.client_id,
        user_id=request.user.id,
    )
    return tok


def load_user(username, password, client, request, *args, **kwargs):
    if request.body.get('weixin_connect'):
        grant_code = password
        u = weixin_login(grant_code)
    else:
        u = phone_login(username, password)
    g.loaded_user = u
    return u


def init_oauth2_provider(provider):
    provider.clientgetter(load_client)
    provider.grantgetter(load_grant)
    provider.grantsetter(save_grant)
    provider.tokengetter(load_token)
    provider.tokensetter(save_token)
    provider.usergetter(load_user)
