# coding=utf-8

import sys
from datetime import datetime, timedelta

from werkzeug.utils import cached_property
from flask import request
from flask_restful import fields
from flask_restful_swagger import swagger

import pp.lib.exc as exc
from pp.lib.mq import make_async
from pp.lib.logger import applog
from pp.model.user.notification import push_notify_user
from pp.lib.sqlstore import db, create_service, IntegrityError, TINYINT
from pp.lib.caching import CacheableMixin, create_redis_region, WEEK, DAY
from pp.lib.loc import get_distance_between_coords, distance_summary
from pp.api.resources.fields import LocationField, DistField, NameField
from pp.model.user.models import users, User
from pp.model.user.settings import bit_settings
from pp.model.location.models import locations, Location

HALL_SIZE = 20

@swagger.model
class RetailerModel(object):
    resource_fields = {
        'id': fields.Integer,
        'name': fields.String,
    }


RETAILER_ORDER = [
    u'影院票台',
    u'美团猫眼',
    u'格瓦拉',
    u'微信电影票',
    u'大众点评',
    u'淘宝电影',
    u'百度糯米',
    u'影院官方平台',
    u'时光网',
    u'QQ电影票',
    u'豆瓣电影',
    u'网票网',
    u'抠电影',
    u'蜘蛛网',
    u'卖座网',
    u'院线通',
    u'其他',
]

ALL_RETAILERS = [
    {
        'id': i,
        'name': name,
    } for i, name in [
        (1, u'美团猫眼'),
        (2, u'格瓦拉'),
        (3, u'微信电影票'),
        (4, u'大众点评'),
        (5, u'淘宝电影'),
        (6, u'百度糯米'),
        (7, u'影院官方平台'),
        (8, u'时光网'),
        (9, u'QQ电影票'),
        (10, u'豆瓣电影'),
        (11, u'网票网'),
        (12, u'抠电影'),
        (13, u'蜘蛛网'),
        (14, u'卖座网'),
        (15, u'院线通'),
        (50, u'影院票台'),
        (99, u'其他'),
    ]
]
ALL_RETAILERS.sort(key=lambda r: RETAILER_ORDER.index(r['name']) if r['name'] in RETAILER_ORDER else sys.maxint)


@swagger.model
class CinemaModel(object):
    resource_fields = {
        'id': fields.Integer,
        'name': fields.String,
        'location': LocationField(attribute='loc_id'),
        'addr': fields.String,
        'tel': fields.String,
        'dist': DistField(),
        'fav': fields.String,
        'lat': fields.Float,
        'lng': fields.Float,
    }


class Cinema(CacheableMixin, db.Model):
    cache_region = create_redis_region(WEEK)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), nullable=False)
    loc_id = db.Column(db.Integer, nullable=False)
    tel = db.Column(db.String(30), default='', nullable=False)
    lng = db.Column(db.Float, default=0.0, nullable=False)
    lat = db.Column(db.Float, default=0.0, nullable=False)
    addr = db.Column(db.String(100), default='', nullable=False)
    mt_id = db.Column(db.Integer, index=True, unique=True)
    edited = db.Column(TINYINT, default=0, nullable=False)
    flag = db.Column(TINYINT, default=0, nullable=False)

    # loc = db.relationship(Location, primaryjoin='Location.id == foreign(Cinema.loc_id)')

    @cached_property
    def loc(self):
        return locations.get(self.loc_id)

    __table_args__ = {'mysql_charset': 'utf8'}

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @classmethod
    def gets_by(cls, city, q=None, lat=None, lng=None):
        objs = cls.orm.findall(loc_id=city.id)
        if q:
            objs = filter(lambda obj: q.lower() in obj.name.lower(), objs)
        if lat and lng:
            pass  # TODO: sort by distance
        return objs

    @cached_property
    def fav(self):
        if not request.user:
            return '0'
        return '1' if self.id in favcinemas.get_cinema_ids_by_user(request.user.id) else '0'

    def dist(self):
        if not self.lat or not self.lng or not request.lat or not request.lng:
            return sys.maxint
        return get_distance_between_coords((request.lat, request.lng), (self.lat, self.lng))

    def dist_desc(self):
        d = self.dist()
        if d == sys.maxint:
            return ''
        return distance_summary(d)

    @classmethod
    def get_visited_user_ids(cls, cinema_id):
        user_ids = [r.Ticket.user_id for r in db.session.query(Ticket,Show).filter(Ticket.show_id == Show.id).filter(Show.cinema_id == cinema_id).all()]
        return user_ids

cinemas = create_service(Cinema)


class FavCinema(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    cinema_id = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('user_id', 'cinema_id'),
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @classmethod
    def get_cinema_ids_by_user(cls, user_id):
        cinema_ids = [o.cinema_id for o in cls.orm.findall(user_id=user_id)]
        return cinema_ids

    @classmethod
    def get_cinemas_by_user(cls, user_id):
        cinema_ids = cls.get_cinema_ids_by_user(user_id)
        return cinemas.gets(cinema_ids)

    @classmethod
    def get_count_by_user(cls, user_id):
        return len(cls.get_cinema_ids_by_user(user_id))

favcinemas = create_service(FavCinema)


@swagger.model
class MovieModel(object):
    resource_fields = {
        'id': fields.Integer,
        'name': fields.String,
        'poster': fields.String(attribute='mt_cover'),
    }


@swagger.model
class MovieFullModel(object):
    resource_fields = dict(MovieModel.resource_fields)
    resource_fields.update({
        'intro': fields.String,
        'rating': fields.String(attribute='show_rating'),
        'tags': fields.String(attribute='show_attrs'),  # to remove
        'attrs': fields.String(attribute='show_attrs'),
        'en_name': fields.String,  # to remove
        'full_name': fields.String(attribute='show_full_name'),
        'pub_year': fields.String(attribute='show_year'),
        'duration': fields.Integer,
    })


class Movie(CacheableMixin, db.Model):
    cache_region = create_redis_region(WEEK)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    en_name = db.Column(db.String(40), default='', nullable=False)
    pub_date = db.Column(db.Date, default=0, nullable=False)
    poster_id = db.Column(db.Integer, default=0, nullable=False)
    rating = db.Column(db.SmallInteger, default=0, nullable=False)  # x10
    intro = db.Column(db.String(1000), default='', nullable=False)
    attrs = db.Column(db.String(200), default='', nullable=False)
    year = db.Column(db.SmallInteger, default=0, nullable=False)
    mt_id = db.Column(db.Integer, index=True, unique=True)
    mt_cover = db.Column(db.String(100))  # TODO: upload to cdn
    show_count = db.Column(db.Integer, default=0, nullable=False)
    edited = db.Column(TINYINT, default=0, nullable=False)

    __table_args__ = {'mysql_charset': 'utf8'}

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def show_rating(self):
        if self.rating:
            return '%.1f' % (self.rating / 10.0)
        return ''

    @cached_property
    def show_full_name(self):
        return u' '.join(
            filter(None, [self.name, self.en_name, u'(%s)' % self.year if self.year else u'']),
        )

    @cached_property
    def poster_url(self):
        return self.mt_cover

    @cached_property
    def tags(self):
        for attr in self.attrs.split():
            if attr.startswith(u'类型:'):
                return attr.split(':')[1]
        return ''

    def _get_type_attr(self, t):
        for attr in self.attrs.split():
            if attr.startswith(t):
                return attr.split(':')[1]
        return ''

    @cached_property
    def show_attrs(self):
        lst = [u'%s%s' % (self.pub_date.strftime('%Y-%m-%d'), u'上映')]
        for k in (u'时长', u'类型', u'地区', u'导演', u'主演'):
            v = self._get_type_attr(k)
            v = v.split(u',')
            if k == u'导演':
                v = [u'%s(导演)' % _v for _v in v]
            if k == u'主演':
                v = v[:3]
            lst.extend(v)
        return u' / '.join(lst)

    @cached_property
    def duration(self):
        v = self._get_type_attr(u'时长')
        if v and v.endswith(u'分钟'):
            return int(v[:-2].strip())
        return 0

    @cached_property
    def show_year(self):
        if self.year:
            return str(self.year)
        if self.pub_date:
            return str(self.pub_date.year)
        return ''

    @classmethod
    def onshow_movie_ids_func(cls):

        @cls.cache_region.cache_on_arguments()
        def _onshow_movie_ids_func():
            rs = cls.query.with_entities(cls.id).order_by(-cls.show_count)
            return [m.id for m in rs]

        return _onshow_movie_ids_func

    @classmethod
    def onshow_movies(cls):
        get_ids = cls.onshow_movie_ids_func()
        return cls.orm.gets(get_ids())

    @classmethod
    def update_onshow_movies(cls):
        get_ids = cls.onshow_movie_ids_func()
        get_ids.refresh()

    @classmethod
    def get_watched_user_ids(cls, movie_id):
        user_ids = [r.Ticket.user_id for r in db.session.query(Ticket,Show).filter(Ticket.show_id == Show.id).filter(Show.movie_id == movie_id).all()]
        return user_ids

movies = create_service(Movie)


@swagger.model
@swagger.nested(
    movie=MovieModel.__name__,
    cinema=CinemaModel.__name__,
)
class ShowModel(object):
    resource_fields = {
        'id': fields.Integer,
        'date': fields.String,
        'time': fields.String,
        'movie': fields.Nested(MovieModel.resource_fields),
        'cinema': fields.Nested(CinemaModel.resource_fields),
        'hall': fields.String,
        'chatroom_id': fields.String(attribute='id'),
    }


class ShowHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False)
    time = db.Column(db.DateTime, nullable=False)
    movie_id = db.Column(db.Integer, nullable=False)
    cinema_id = db.Column(db.Integer, nullable=False)
    hall = db.Column(db.String(HALL_SIZE), default='', nullable=False)

    __table_args__ = (
        db.UniqueConstraint('cinema_id', 'date', 'movie_id', 'time', 'hall'),
        {'mysql_charset': 'utf8'},
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)


class Show(CacheableMixin, db.Model):
    cache_region = create_redis_region(DAY * 3)
    cache_query_fields = [
        ('cinema_id', 'date'),
        ('cinema_id', 'movie_id'),  # FIXME
    ]

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False)
    time = db.Column(db.DateTime, nullable=False)
    movie_id = db.Column(db.Integer, nullable=False)
    cinema_id = db.Column(db.Integer, nullable=False)
    hall = db.Column(db.String(HALL_SIZE), default='', nullable=False)

    __table_args__ = (
        db.UniqueConstraint('cinema_id', 'date', 'movie_id', 'time', 'hall'),
        {'mysql_charset': 'utf8'},
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def movie(self):
        return movies.get(self.movie_id)

    @cached_property
    def movie_pub_year(self):
        return self.movie.show_year

    @cached_property
    def cinema(self):
        return cinemas.get(self.cinema_id)

    def time_in(self):
        return calc_time_in(self.time)


def calc_time_in(time):
    now = datetime.now()
    if now >= time:
        return u'已放映'
    d = time - now
    if d.days:
        return u'%d天' % d.days
    elif d.seconds > 3600:
        return u'%d小时' % (d.seconds / 3600)
    else:
        return u'%d分钟' % (d.seconds / 60)


shows = create_service(Show)


@swagger.model
class TicketModel(object):
    '''Ticket'''
    resource_fields = {
        'id': fields.Integer,
        'movie': NameField(attribute='movie'),
        'movie_en_name': fields.String(attribute='movie.en_name'),
        'movie_pub_year': fields.String(attribute='movie_pub_year'),
        'movie_poster': fields.String(attribute='movie.mt_cover'),
        'movie_id': fields.Integer,
        'cinema': NameField(attribute='cinema'),
        'cinema_id': fields.Integer,
        'date': fields.String,
        'time': fields.String,
        'hall': fields.String,
        'seats': fields.String,
        'price': fields.String(attribute='show_price'),  # 50.00 5000
        'fee': fields.String(attribute='show_fee'),  # 5.0 500
        'retailer': fields.String(attribute='retailer_name'),
        'show_id': fields.Integer,
        'chatroom_id': fields.Integer,
    }


class Ticket(CacheableMixin, db.Model):
    cache_region = create_redis_region(WEEK)
    cache_query_fields = [
        ('show_id', 'user_id'),
    ]
    cache_version = '1'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    show_id = db.Column(db.Integer, nullable=False)
    hall = db.Column(db.String(HALL_SIZE), default='', nullable=False)
    seats = db.Column(db.String(40), default='', nullable=False)
    price = db.Column(db.Integer, default=0, nullable=False)
    fee = db.Column(db.Integer, default=-1, nullable=False)
    retailer_id = db.Column(db.Integer, default=0, nullable=False)
    sms = db.Column(db.String(100), default='', nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    date = db.Column(db.Date, nullable=False)
    time = db.Column(db.DateTime, nullable=False)
    movie_id = db.Column(db.Integer, nullable=False)
    cinema_id = db.Column(db.Integer, nullable=False)

    # user = db.relationship(User, primaryjoin='User.id == foreign(Ticket.user_id)')
    show = db.relationship(Show, primaryjoin='Show.id == foreign(Ticket.show_id)', lazy='subquery')

    @cached_property
    def user(self):
        return users.get(self.user_id)

    __table_args__ = (
        db.UniqueConstraint('user_id', 'show_id', name='user_show'),
        {'mysql_charset': 'utf8'},
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def cinema(self):
        return cinemas.get(self.cinema_id)

    @cached_property
    def movie(self):
        return movies.get(self.movie_id)

    @cached_property
    def movie_pub_year(self):
        return self.movie.show_year

    @cached_property
    def chatroom_id(self):
        return self.show_id

    @cached_property
    def retailer_name(self):
        d = [r for r in ALL_RETAILERS if r['id'] == self.retailer_id]
        return d[0]['name'] if d else ''

    @cached_property
    def show_fee(self):
        return str(self.fee) if self.fee != -1 and self.fee != 0 else ''

    @cached_property
    def show_price(self):
        return str(self.price) if self.price else ''

    @classmethod
    def get_count_by_user(cls, user_id):
        return len(cls.orm.findall(user_id=user_id))

    def time_in(self):
        return calc_time_in(self.time)

    @classmethod
    def add_by_show_id(cls, user_id, show_id, **details):
        applog('addtickets', 'user_id=%s show_id=%s', user_id, show_id)
        show = shows.get(show_id)
        if not show:
            return None
        try:
            ticket = cls.orm.create(
                user_id=user_id,
                show_id=show.id,
                hall=show.hall,
                price=details.get('price'),
                fee=details.get('fee', -1),
                retailer_id=details.get('retailer_id', 0),
                seats=details.get('seats', ''),
                date=show.date,
                time=show.time,
                cinema_id=show.cinema_id,
                movie_id=show.movie_id,
            )
            from pp.model.chat.models import new_message, MessageType
            new_message(user_id, show.id, u'%s 加入了聊天室' % request.user.nickname, type=MessageType.join)
            ticket.add_notify_jobs()
            return ticket
        except IntegrityError:
            ticket = cls.orm.first(user_id=user_id, show_id=show.id)
            ticket.update_detail(**details)
            return ticket

    def update_detail(self, **details):
        to_update = {}
        for field in ('hall', 'price', 'fee', 'seats', 'retailer_id'):
            if field in details:
                to_update[field] = details[field]

        if to_update:
            self.orm.update(self, **to_update)

    def add_notify_jobs(self):
        try:
            duration = self.movie.duration
            assert duration != 0, 'duration is 0'
        except Exception as e:
            applog('ticketnoti', 'ticket_id=%s error=%s detail=invalid_movie_duration', self.id, e)
            duration = 0
        open_delay = (self.time - datetime.now()).total_seconds() - 600
        if open_delay > 0:
            applog('ticketnoti', 'ticket_id=%s delay=%s type=notify_open', self.id, open_delay)
            notify_ticket_open.async(delay=open_delay, priority=100)(self.id)
        if duration != 0:
            close_delay = (self.time - datetime.now()).total_seconds() + duration * 60 + 900
            if close_delay > 0:
                applog('ticketnoti', 'ticket_id=%s delay=%s type=notify_close', self.id, close_delay)
                notify_ticket_close.async(delay=close_delay, priority=100)(self.id)

tickets = create_service(Ticket)


@make_async()
def notify_ticket_open(ticket_id):
    t = tickets.get(ticket_id)
    if not t:
        applog('ticketnoti', 'ticket not found: notify_ticket_open abort. id=%s', ticket_id)
        return
    settings = bit_settings.get_user_settings(t.user_id)
    if not settings['noti_show']:
        applog('ticketnoti', 'user %d set don\'t noti_show', t.user_id)
        return
    s = u'电影《%s》快要开始啦，请前往%s，凭票排队入场。' % (t.movie.name, t.hall)
    push_notify_user.async()(t.user_id, s)


@make_async()
def notify_ticket_close(ticket_id):
    t = tickets.get(ticket_id)
    if not t:
        applog('ticketnoti', 'ticket not found: notify_ticket_close abort. id=%s', ticket_id)
        return
    settings = bit_settings.get_user_settings(t.user_id)
    if not settings['noti_discuss_after_show']:
        applog('ticketnoti', 'user %d set don\'t noti_discuss_after_show', t.user_id)
        return
    s = u'看完《%s》，不想跟同场的人聊点什么？' % (t.movie.name,)
    push_notify_user.async()(t.user_id, s)


def distinct(lst):
    l = []
    s = set()
    for v in lst:
        if v not in s:
            l.append(v)
            s.add(v)
    return l


def user_visited_cinemas(user_id):
    ts = tickets.find(user_id=user_id)[::-1]
    cinema_ids = distinct([t.cinema_id for t in ts])
    return cinemas.gets(cinema_ids)


def user_watched_movies(user_id):
    ts = tickets.find(user_id=user_id)[::-1]
    movie_ids = distinct([t.movie_id for t in ts])
    return movies.gets(movie_ids)
