# coding=utf-8

import sys

from flask_restful_swagger import swagger
from flask_restful import fields

from pp.lib.sqlstore import db, create_service, TINYINT
from pp.lib.caching import CacheableMixin, create_redis_region, MONTH
from pp.lib.loc import get_nearest

HOT_ORDER = u'北京 上海 广州 深圳 成都 武汉 重庆 杭州 南京 苏州 西安 天津'.split()


class LocType(object):
    country_or_region = 1
    province = 2
    city = 3
    district = 4
    village_town = 5

loctype_names = {
    LocType.country_or_region: u'国家地区',
    LocType.province: u'省',
    LocType.city: u'市',
    LocType.district: u'区县',
    LocType.village_town: u'村镇',
}


@swagger.model
class LocationModel(object):
    resource_fields = {
        'id': fields.Integer,
        'name': fields.String,
        'en_name': fields.String,
    }


class Location(CacheableMixin, db.Model):
    cache_region = create_redis_region(MONTH)
    cache_query_fields = [
        ('name', 'type'),
        ('en_name', 'type'),
    ]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(6), nullable=False)
    en_name = db.Column(db.String(20), nullable=False)
    hot = db.Column(TINYINT, default=0, nullable=False)
    mt_abbv = db.Column(db.String(20), default='', nullable=False)
    parent_id = db.Column(db.Integer, default=0, nullable=False)
    type = db.Column(TINYINT, nullable=False)
    lat = db.Column(db.Float, default=0.0, nullable=False)
    lng = db.Column(db.Float, default=0.0, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('name', 'type'),
        {'mysql_charset': 'utf8'},
    )

    def __repr__(self):
        return u'%s<id=%d name=%s type=%s>' % (self.__class__.__name__, self.id, self.name, loctype_names[self.type])

    def is_hot(self):
        return self.hot == 1

    @classmethod
    def get_beijing(cls):
        return cls.search_city(u'北京')

    @classmethod
    def search_city(cls, q):
        return cls._search_type(q, LocType.city)

    @classmethod
    def search_province(cls, q):
        return cls._search_type(q, LocType.province)

    @classmethod
    def _search_type(cls, q, type):
        if isinstance(q, int):
            return cls.orm.get(q)
        q = unicode(q)
        try:
            q.encode('ascii')
        except UnicodeEncodeError:
            return cls.orm.first(name=q, type=type)
        else:
            return cls.orm.first(en_name=q, type=type)

    @classmethod
    def guess_by_lat_lng(cls, (lat, lng)):
        l, dis = get_nearest((lat, lng), cls.all_cities())
        return l

    @classmethod
    def all_cities(cls):
        return cls.orm.findall(type=LocType.city)

    @classmethod
    def hot_cities(cls):
        hot_cities = [c for c in cls.all_cities() if c.is_hot()]
        hot_cities.sort(key=lambda c: HOT_ORDER.index(c.name) if c.name in HOT_ORDER else sys.maxint)
        return hot_cities

locations = create_service(Location)
