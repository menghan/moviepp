# coding=utf-8

from datetime import datetime

from werkzeug.utils import cached_property

from pp.lib.sqlstore import db, create_service, TINYINT
from pp.model.user.models import users


class Report(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False, default='')
    user_id = db.Column(db.Integer, nullable=False, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    __table_args__ = (
        {'mysql_charset': 'utf8mb4'},
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def user(self):
        return self.user_id and users.get(self.user_id)

reports = create_service(Report)
