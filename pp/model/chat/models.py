# coding=utf-8

from datetime import datetime, timedelta
import json
import sys
import re

import redis
from flask import current_app, request
from flask_restful import marshal, fields
from flask_restful_swagger import swagger
from werkzeug.utils import cached_property

import pp.lib.exc as exc
from pp.api.resources.fields import StaticField, JoinField, LocationField
from pp.lib.sqlstore import db, create_service, TINYINT, BigInteger
from pp.lib.mq import make_async
from pp.lib.logger import applog
from pp.lib.caching import CacheableMixin, create_redis_region, DAY
from pp.lib.perf import lazy_to_batch
from pp.model.show.models import shows, tickets, movies
from pp.model.user.models import users, UserModel
from pp.model.user.notification import notifications, mentions
from pp.model.location.models import locations

Pool = None


@swagger.model
class BuddyModel(object):
    resource_fields = dict(UserModel.resource_fields)
    resource_fields.update({
        'seats': fields.String,
    })


@swagger.model
class ChatRoomModel(object):
    resource_fields = {
        'id': fields.Integer,
        'preview': fields.String,
        'joined': JoinField,
        'show_time': fields.String,
        'poster': fields.String(attribute='movie_poster'),
        'cinema_name': fields.String,
        'movie_id': fields.String,
        'movie_name': fields.String,
        'movie_pub_year': fields.String,
        'hall': fields.String(attribute='hall_name'),
        'show_in': fields.String(attribute='show_in'),
        'last_message_time': fields.String,
        'chatme': fields.String(attribute='has_at_me'),
        'duration': fields.Integer,
    }


@swagger.model
@swagger.nested(
    other=UserModel.__name__,
)
class PrivChatModel(object):
    resource_fields = {
        'id': fields.Integer,
        'preview': fields.String,
        'other': fields.Nested(UserModel.resource_fields),
        'last_message_time': fields.String,
    }


@swagger.model
@swagger.nested(
    other=UserModel.__name__,
)
class ChatUnitModel(object):
    resource_fields = {
        'id': fields.Integer,
        'preview': fields.String,
        'joined': JoinField,
        'show_time': fields.String,
        'poster': fields.String(attribute='movie_poster'),
        'cinema_name': fields.String,
        'movie_id': fields.String,
        'movie_name': fields.String,
        'movie_pub_year': fields.String,
        'hall': fields.String(attribute='hall_name'),
        'show_in': fields.String(attribute='show_in'),
        'last_message_time': fields.String,
        'chatme': fields.String(attribute='has_at_me'),
        'other': fields.Nested(UserModel.resource_fields),
        'type_str': fields.String,
    }


@swagger.model
@swagger.nested(
    user=UserModel.__name__,
)
class MessageModel(object):
    resource_fields = {
        'id': fields.Integer,
        'chatroom_id': fields.Integer,
        'privchat_id': fields.Integer(attribute='priv_other_id'),
        'created_at': fields.String,
        'user': fields.Nested(UserModel.resource_fields),
        'payload': fields.String,
        'type': fields.String,
        'seats': fields.String,
        'random_id': fields.Integer,
    }


@swagger.model
@swagger.nested(
    newest_messages=MessageModel.__name__,
)
class ChatRoomFullModel(object):
    resource_fields = dict(ChatRoomModel.resource_fields)
    resource_fields.update({
        'newest_messages': fields.List(fields.Nested(MessageModel.resource_fields)),
    })


class ChatUnitMixin(object):

    @property
    def other(self):
        pass

    @property
    def joined(self):
        pass

    @property
    def show_time(self):
        pass

    @property
    def movie_poster(self):
        pass

    @property
    def movie_pub_year(self):
        pass

    @property
    def cinema_name(self):
        pass

    @property
    def movie_id(self):
        pass

    @property
    def movie_name(self):
        pass

    @property
    def hall_name(self):
        pass

    @property
    def show_in(self):
        pass

    @property
    def has_at_me(self):
        pass

    @property
    def duration(self):
        pass


class ChatRoom(ChatUnitMixin):

    def __init__(self, show_id):
        self.id = show_id
        self.ticket = None
        self.show = shows.get(show_id)
        if self.show is None:
            self.ticket = tickets.first(show_id=show_id)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @property
    def type_str(self):
        return 'chatroom'

    @cached_property
    def has_at_me(self):
        if request.user:
            return '1' if mentions.first(user_id=request.user.id, chatroom_id=self.id) else '0'
        return '0'

    def remove_at_me(self):
        if request.user:
            atme = mentions.first(user_id=request.user.id, chatroom_id=self.id)
            if atme:
                atme.orm.delete(atme)

    @classmethod
    def get(cls, id):
        return cls(id)

    @classmethod
    def gets(cls, ids):
        return map(cls, ids)

    @cached_property
    def movie_id(self):
        return self.show.movie_id if self.show else self.ticket.movie_id

    @cached_property
    def movie(self):
        return movies.get(self.movie_id)

    @cached_property
    def movie_name(self):
        return self.movie.name

    @cached_property
    def cinema(self):
        return self.show.cinema if self.show else self.ticket.cinema

    @cached_property
    def cinema_name(self):
        return self.cinema.name

    @cached_property
    def movie_poster(self):
        return self.movie.poster_url

    @cached_property
    def movie_pub_year(self):
        return self.movie.show_year

    @cached_property
    def duration(self):
        return self.movie.duration

    @cached_property
    def hall_name(self):
        return self.show.hall if self.show else self.ticket.hall

    @cached_property
    def show_in(self):
        return self.show.time_in() if self.show else self.ticket.time_in()  # TODO

    @cached_property
    def show_time(self):
        return self.show.time if self.show else self.ticket.time

    @classmethod
    def get_last_message(cls, chatroom_id):
        last_msg = lastmessages.get(chatroom_id)
        if last_msg:
            return messages.get(last_msg.message_id)

    @cached_property
    def last_msg(self):
        return self.get_last_message(self.id)

    @cached_property
    def preview(self):
        last_msg = self.last_msg
        return last_msg.preview if last_msg else u'快来抢个沙发吧!'

    @cached_property
    def last_message_time(self):
        last_msg = self.last_msg
        return last_msg.relative_time if last_msg else u''

    def is_joined_by(self, user_id):
        return self.id in [t.show_id for t in tickets.findall(user_id=user_id)]

    @classmethod
    def gets_ids_by_user(cls, user_id):
        objs = tickets.findall(user_id=user_id)
        objs.sort(key=lambda obj: obj.created_at or datetime(2000, 1, 1), reverse=True)
        chatroom_ids = show_ids = [obj.show_id for obj in objs]
        return chatroom_ids

    @classmethod
    def gets_by_user(cls, user_id):
        return cls.gets(cls.gets_ids_by_user(user_id))

    @cached_property
    def buddies(self):
        objs = tickets.findall(show_id=self.id)
        us = []
        for obj in objs:
            u = users.get(obj.user_id)
            u.seats = obj.seats
            us.append(u)
        return us

    @cached_property
    def newest_messages(self):
        return Message.history(type=MessageType.normal,
                               owner_id=None,  # owner_id
                               other_id=None,  # other_id
                               chatroom_id=self.id,
                               limit=10)

    @cached_property
    def updated_at(self):
        if self.last_msg:
            return self.last_msg.created_at
        return datetime(2000, 1, 1)

chatrooms = ChatRoom


class PrivChat(ChatUnitMixin):

    def __init__(self, owner, other):
        self.id = self.other_id = other.id
        self.owner = owner

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @property
    def other(self):
        return users.get(self.other_id)

    @property
    def type_str(self):
        return 'privchat'

    @cached_property
    def has_at_me(self):
        return '0'

    def remove_at_me(self):
        pass

    def is_joined_by(self, user_id):
        return True

    @classmethod
    def get(cls, owner_id, other_id):
        return cls(users.get(owner_id), users.get(other_id))

    def get_last_message(self):
        ms = Message.history(type=MessageType.priv, owner_id=self.owner.id,
                             other_id=self.other.id, chatroom_id=None, limit=1)
        if ms:
            return ms[0]

    @cached_property
    def last_msg(self):
        return self.get_last_message()

    @cached_property
    def preview(self):
        last_msg = self.last_msg
        return last_msg.preview if last_msg else u''

    @cached_property
    def last_message_time(self):
        last_msg = self.last_msg
        return last_msg.relative_time if last_msg else u''

    @cached_property
    def updated_at(self):
        return self.last_msg.created_at

    @classmethod
    def gets_other_ids_by_user(cls, user_id):
        objs = messages.query.filter_by(priv_owner_id=user_id).with_entities(messages.priv_other_id).order_by(messages.created_at).distinct()

        def distinct(lst):
            s = set()
            r = []
            for v in lst:
                if v not in s:
                    r.append(v)
                    s.add(v)
            return r

        other_ids = [obj.priv_other_id for obj in distinct(objs)]
        return other_ids

    @classmethod
    def gets_by_user(cls, user_id):
        user = users.get(user_id)
        other_ids = cls.gets_other_ids_by_user(user_id)
        return [cls(user, users.get(other_id)) for other_id in other_ids]

    @cached_property
    def newest_messages(self):
        return Message.history(type=MessageType.normal, owner_id=self.owner.id,
                               other_id=self.other.id, chatroom_id=None, limit=10)

privchats = PrivChat


class MessageType(object):
    normal = 0
    join = 2
    priv = 3


class Message(CacheableMixin, db.Model):
    cache_region = create_redis_region(DAY * 2)

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)  # author
    priv_owner_id = db.Column(db.Integer, index=True, default=0, nullable=False)  # inbox owner_id if type is priv
    priv_other_id = db.Column(db.Integer, default=0, nullable=False)  # inbox target_id if type is priv
    chatroom_id = db.Column(db.Integer, default=0, index=True, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    type = db.Column(TINYINT, default=MessageType.normal, nullable=False)
    payload2 = db.Column(db.Text, nullable=False)
    random_id = db.Column(BigInteger, nullable=False, default=0)

    __table_args__ = {'mysql_charset': 'utf8mb4'}

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def payload(self):
        return self.payload2

    @cached_property
    def user(self):
        return users.get(self.user_id)

    @cached_property
    def preview(self):
        if self.type == MessageType.normal:
            return u'%s: %s' % (self.user.nickname, self.payload)
        else:
            return self.payload

    @cached_property
    def seats(self):
        if self.type == MessageType.priv:
            return ''
        ticket = tickets.first(user_id=self.user_id, show_id=self.chatroom_id)
        return ticket.seats if ticket else u''

    def create_mqtt_payload(self):
        return json.dumps(marshal(self, MessageModel.resource_fields))

    @classmethod
    def history(cls, type, owner_id, other_id, chatroom_id, max_id=sys.maxint, limit=20):
        if type == MessageType.priv:
            q = cls.query.filter_by(type=type, priv_owner_id=owner_id, priv_other_id=other_id)
        else:
            q = cls.query.filter_by(chatroom_id=chatroom_id).filter(cls.type.in_((MessageType.normal, MessageType.join)))
        ms = q.filter(cls.id<max_id).order_by(-cls.id).limit(limit).all()
        ms.reverse()
        return ms

    @cached_property
    def relative_time(self):
        now = datetime.now()
        diff = now.date() - self.created_at.date()
        if diff.days >= 6:
            return self.created_at.strftime('%y/%m/%d')
        if diff.days > 1:
            return u'星期%s' % weekchday(self.created_at.weekday())
        if diff.days == 1:
            return u'昨天'
        time_desc = get_time_desc(self.created_at)
        return u'%s%s' % (time_desc, self.created_at.strftime('%H:%M'))

    def publish(self):
        global Pool
        if Pool is None:
            Pool = redis.BlockingConnectionPool.from_url(current_app.config['MQTT_REDIS_URI'])

        r = redis.Redis(connection_pool=Pool)
        payload = self.create_mqtt_payload()
        if self.type == MessageType.normal:
            r.publish(build_topic(self.chatroom_id), payload)
        r.publish(build_user_topic(self.priv_owner_id), payload)

messages = create_service(Message)


def weekchday(n):
    s = u'日一二三四五六'
    return s[n]


def get_time_desc(t):
    return [u'凌晨', u'上午', u'下午', u'晚上'][t.hour / 6]


def build_topic(chatroom_id):
    return 'chatroom_%s' % chatroom_id


def build_user_topic(user_id):
    return 'user_%s' % user_id


def parse_chatroom_id(topic):
    chatroom_prefix = 'chatroom_'
    if not topic.startswith(chatroom_prefix):
        return
    chatroom_id = topic[len(chatroom_prefix):]
    return chatroom_id


@make_async()
def handle_msg(user_id, topic, payload):
    applog('handle_msg', 'accept job user_id=%r, topic=%r, payload=%r', user_id, topic, payload)
    chatroom_id = parse_chatroom_id(topic)
    if not user_id.isdigit():
        return
    user_id = int(user_id)
    chatroom_id = int(chatroom_id)
    if not user_id or not chatroom_id:
        return
    if not tickets.first(user_id=user_id, show_id=chatroom_id):
        return
    try:
        d = json.loads(payload)
        payload = d['payload']
        random_id = int(d.get('random_id', 0))
    except Exception as e:
        applog('handle_msg', 'invalid msg payload=%r: %s', payload, e)
        return
    new_message(user_id, chatroom_id, payload, MessageType.normal, random_id)

MENTION_PERSION = ur'@([^ ]*) '


def check_recent_random_id(user_id, random_id):
    if random_id == 0:
        return True
    retry_threshold = timedelta(days=1)
    ms = messages.findall(user_id=user_id, random_id=random_id)
    if ms and datetime.now() - sorted(ms, key=lambda m: m.id)[-1].created_at < retry_threshold:
        return False
    return True


def new_message(user_id, chatroom_id, payload, type, random_id=0):
    duplicated = not check_recent_random_id(user_id, random_id)
    applog('handle_msg', 'user_id=%s chatroom_id=%s random_id=%s duplicated=%s', user_id, chatroom_id, random_id, duplicated)
    if duplicated:
        return
    msg = messages.create(
        user_id=user_id,
        chatroom_id=chatroom_id,
        payload2=payload,
        random_id=random_id,
        type=type,
    )
    applog('handle_msg', 'message created message_id=%s', msg.id)
    lastmessages.update_chatroom(chatroom_id, msg.id)
    nicknames = set(re.findall(MENTION_PERSION, payload))
    mentioned = set()
    if nicknames:
        objs = tickets.findall(show_id=chatroom_id)
        for obj in objs:
            if obj.user.nickname in nicknames:
                mentioned.add(obj.user_id)
    mentioned.discard(user_id)

    for mention_user_id in mentioned:
        db.session.merge(notifications.new(id=mention_user_id, has_at_me=1))
    if mentioned:
        db.session.commit()
    for mention_user_id in mentioned:
        mentions.create_and_notify(user_id=mention_user_id, chatroom_id=chatroom_id)

    msg.publish()
    return msg


def new_priv_message(user_id, other_id, payload, random_id=0):
    if not check_recent_random_id(user_id, random_id):
        return
    msg = messages.new(
        user_id=user_id,
        priv_owner_id=user_id,
        priv_other_id=other_id,
        payload2=payload,
        random_id=random_id,
        type=MessageType.priv,
    )
    other_msg = messages.new(
        user_id=user_id,
        priv_owner_id=other_id,
        priv_other_id=user_id,
        payload2=payload,
        random_id=random_id,
        type=MessageType.priv,
    )
    db.session.add(msg)
    db.session.add(other_msg)
    db.session.commit()
    msg.publish()
    other_msg.publish()
    return msg


@swagger.model
class ChatRoomUpdateModel(object):
    resource_fields = {
        'chatroom_id': fields.Integer(attribute='id'),
        'preview': fields.String(attribute='message.preview'),
        'last_message_time': fields.String(attribute='message.relative_time'),
    }


def get_messages(akws):
    message_ids = []
    for a, kw in akws:
        message_ids.append(a[0].message_id)
    messages.gets(message_ids)


class LastMessage(CacheableMixin, db.Model):
    cache_region = create_redis_region(DAY * 2)

    id = db.Column(db.Integer, primary_key=True)  # chatroom_id
    message_id = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '%s<id=%d message_id=%d>' % (self.__class__.__name__, self.id, self.message_id)

    @property
    def message(self):
        return messages.get(self.message_id)

    @classmethod
    def update_chatroom(cls, chatroom_id, message_id):
        db.session.merge(cls.orm.new(id=chatroom_id, message_id=message_id))
        db.session.commit()

    @lazy_to_batch(get_messages)
    def to_dict(self):
        d = self.__dict__
        d.update({
            'message': self.message,
        })
        return d

lastmessages = create_service(LastMessage)
