# coding=utf-8

import logging

from pp.lib.sqlstore import db, create_service
from pp.lib.caching import CacheableMixin, create_region
from dogpile.cache.proxy import ProxyBackend

HOUR = 3600
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)


def log_action(f):
    def _(*a):
        logger.info('CacheBackend: %s%r', f.__name__, a[1:])
        return f(*a)
    return _


class LogProxyBackend(ProxyBackend):

    # pylint: disable=E
    @log_action
    def get(self, key):
        return self.proxied.get(key)

    @log_action
    def set(self, key, value):
        self.proxied.set(key, value)

    @log_action
    def delete(self, key):
        self.proxied.delete(key)

    @log_action
    def get_multi(self, keys):
        return self.proxied.get_multi(keys)

    @log_action
    def set_multi(self, keys):
        self.proxied.set_multi(keys)

    @log_action
    def delete_multi(self, keys):
        self.proxied.delete_multi(keys)
    # pylint: enable=E


in_memory_region = create_region(
    backend='dogpile.cache.memory_pickle',
    expiration_time=HOUR,
    wrap=[LogProxyBackend],
)


class TestM(CacheableMixin, db.Model):
    cache_region = in_memory_region
    cache_query_fields = [
        ('cinema_id', 'movie_id'),
    ]

    id = db.Column(db.Integer, primary_key=True)
    cinema_id = db.Column(db.Integer, index=True, nullable=False)
    movie_id = db.Column(db.Integer, index=True, nullable=False)

testms = create_service(TestM)
