"""
Verify code implement.  Only support login type verification.
"""

import random
from datetime import datetime, timedelta

from pp.lib.sqlstore import db, create_service, TINYINT
import pp.lib.exc as exc


class VerifyCode(db.Model):
    CODE_LEN = 4
    RESEND = timedelta(minutes=1)
    EXPIRE = timedelta(minutes=30)

    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(13), unique=True, index=True, nullable=False)
    code = db.Column(db.SmallInteger, nullable=False)  # store code as smallinteger
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    used = db.Column(TINYINT, default=0, nullable=False)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    def can_resend(self):
        return datetime.now() - self.created_at > self.RESEND

    def not_expired(self):
        return datetime.now() - self.created_at < self.EXPIRE

    @classmethod
    def generate(cls, phone):
        last = cls.orm.first(phone=str(phone))
        # if last and last.used == 0 and not last.can_resend():
        #     raise exc.SendTooFrequentlyError()
        code = _generate_code(cls.CODE_LEN)
        if not last:
            last = cls.orm.create(phone=str(phone), code=int(code))
        else:
            last.code = int(code)
            last.created_at = datetime.now()
            last.used = 0
            cls.orm.save(last)
        return code

    @classmethod
    def verify(cls, phone, code):
        if not code.isdigit():
            return False
        last = cls.orm.first(phone=str(phone), code=int(code))
        if last and last.not_expired():
            last.used = 1
            cls.orm.save(last)
            return True
        return False


def _generate_code(len):
    assert len < 19
    l = range(0, 10)
    random.shuffle(l)
    return ''.join(map(str, l))[:len]

verify_codes = create_service(VerifyCode)
