# coding=utf-8

from datetime import datetime

from pp.lib.sqlstore import db, create_service


class Blacklist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    target_id = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('user_id', 'target_id'),
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

blacklists = create_service(Blacklist)


def in_blacklist(me_id, target_id):
    return bool(blacklists.first(user_id=me_id, target_id=target_id))
