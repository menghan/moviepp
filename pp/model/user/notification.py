# coding=utf-8

from flask import current_app
from werkzeug.utils import cached_property

from pp.lib.sqlstore import db, create_service, TINYINT, IntegrityError
from pp.lib.logger import applog
from pp.lib.mq import make_async

from pp.model.user.settings import bit_settings


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)  # user_id
    has_at_me = db.Column(TINYINT)

    def __repr__(self):
        return '%s<id=%d has_at_me=%s>' % (self.__class__.__name__, self.id, self.has_at_me)

notifications = create_service(Notification)


class AtMe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, index=True)
    chatroom_id = db.Column(db.Integer, index=True)

    __table_args__ = (
        db.UniqueConstraint('user_id', 'chatroom_id'),
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @classmethod
    def create_and_notify(cls, **kw):
        o = cls.orm.create_ignore(**kw)
        o.notify()

    def notify(self):
        push_mention(self.id)

    @cached_property
    def chatroom(self):
        from pp.model.chat.models import chatrooms
        return chatrooms.get(self.chatroom_id)

mentions = create_service(AtMe)


def push_mention(mention_id):
    mention = mentions.get(mention_id)
    if not mention:
        applog('mention', 'mention id=% not found', mention_id)
        return
    settings = bit_settings.get_user_settings(mention.user_id)
    if not settings['noti_at_me']:
        applog('mention', 'user %d set don\'t noti_at_me', mention.user_id)
        return
    push_notify_user.async()(mention.user_id, u'有人在%s聊天室中@你' % mention.chatroom.movie_name)
    applog('mention', 'push to user %d done', mention.user_id)


@make_async()
def push_notify_user(user_id, text):
    from jpush import JPush
    from jpush import audience as jpush_audience
    from jpush import all_ as jpush_platform_all
    from jpush import notification as jpush_notification, ios as ios_notification
    from jpush import alias as jpush_alias
    from jpush import JPushFailure

    apikey, secret = current_app.config['JPUSH_APIKEY'], current_app.config['JPUSH_SECRET']
    if not apikey or not secret:
        applog('jpush', 'no jpush settings')
        return
    jpush = JPush(apikey, secret)
    push = jpush.create_push()
    push.audience = jpush_audience(jpush_alias('user_%d' % user_id))
    badge = '+1'
    if isinstance(text, unicode):
        text = text.encode('utf8')
    push.notification = jpush_notification(alert=text, ios=ios_notification(alert=text, badge=badge))
    push.platform = jpush_platform_all
    try:
        push.send()
    except JPushFailure as e:
        if e.error_code == 1011:
            applog('jpush', 'audience %s not exist', user_id)
            return
        raise
    applog('jpush', 'push to user %d done', user_id)
