import operator

from flask import current_app
from flask_restful_swagger import swagger

from pp.api.resources.fields import BooleanStringField
from pp.lib.sqlstore import db, create_service

setting_map = {
    'noti_show':               0b00000001,
    'noti_discuss_after_show': 0b00000010,
    'noti_special_event':      0b00000100,
    'noti_at_me':              0b00001000,
}
_max_settings =                0b11111111,


@swagger.model
class SettingsModel(object):
    '''User settings for app'''
    resource_fields = {
        k: BooleanStringField
        for k in setting_map
    }


class BitSettings(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=True, index=True)
    val = db.Column(db.Integer, default=0, nullable=False)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    def get_setting(self, name):
        return (self.val & setting_map[name]) != 0

    def set_setting(self, name, val):
        if val:
            self.val |= setting_map[name]
        else:
            self.val &= (setting_map[name] ^ _max_settings)
        self.orm.save(self)

    def get_settings(self):
        return dict((k, self.get_setting(k)) for k in setting_map)

    @classmethod
    def get_default_settings_val(cls):
        return reduce(operator.or_, setting_map.values(), 0)

    @classmethod
    def get_user_settings(cls, user_id):
        s = bit_settings.first(user_id=user_id)
        if not s:
            s = bit_settings.create(user_id=user_id, val=cls.get_default_settings_val())
        return s.get_settings()

    @classmethod
    def set_user_settings(cls, user_id, **kwargs):
        val = reduce(operator.or_, [setting_map[name] if int(kwargs.get(name, '0')) else 0 for name in setting_map], 0)
        s = bit_settings.first(user_id=user_id)
        if s:
            s.val = val
            cls.orm.save(s)
        else:
            s = bit_settings.create(user_id=user_id, val=val)

bit_settings = create_service(BitSettings)
