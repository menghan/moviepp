# coding=utf-8

from datetime import datetime, timedelta

from werkzeug.utils import cached_property
from flask import request
from flask_restful_swagger import swagger
from flask_restful import fields

import pp.lib.exc as exc
from pp.lib.sqlstore import db, create_service
from pp.lib.logger import applog
from pp.model.user.notification import push_notify_user


class Followship(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, index=True, nullable=False)
    target_id = db.Column(db.Integer, index=True, nullable=False)
    remark = db.Column(db.String(length=10), default='', nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('user_id', 'target_id'),
        {'mysql_charset': 'utf8mb4'},
    )

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)


def is_following(from_id, to_id):
    return bool(followships.first(user_id=from_id, target_id=to_id))


def follow(from_id, to_id):
    from pp.model.user.models import users
    applog('follow', 'action=follow from=%s to=%s', from_id, to_id)
    from_user = users.get(from_id)
    if not from_user:
        return
    if not followships.first(user_id=from_id, target_id=to_id):
        followships.create(user_id=from_id, target_id=to_id)
        push_notify_user.async()(to_id, u'%s关注你了，去跟聊几句吧 >>' % from_user.nickname)


def unfollow(from_id, to_id):
    applog('follow', 'action=unfollow from=%s to=%s', from_id, to_id)
    f = followships.first(user_id=from_id, target_id=to_id)
    if f:
        followships.delete(f)


def get_following_user_ids(user_id):
    return [f.target_id for f in sorted(followships.findall(user_id=user_id), key=lambda f: f.created_at, reverse=True)]


def get_followed_user_ids(user_id):
    return [f.user_id for f in sorted(followships.findall(target_id=user_id), key=lambda f: f.created_at, reverse=True)]


followships = create_service(Followship)
