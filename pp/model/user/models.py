# coding=utf-8

from datetime import datetime, timedelta

from werkzeug.utils import cached_property
from flask import request
from flask_login import UserMixin
from flask_restful_swagger import swagger
from flask_restful import fields
from werkzeug.security import generate_password_hash, check_password_hash

import pp.lib.exc as exc
from pp.lib.caching import CacheableMixin, create_redis_region, MONTH
from pp.lib.sqlstore import db, create_service, IntegrityError, TINYINT
from pp.lib.logger import applog
from pp.api.resources.fields import StaticField, LocationField, UserDistField
from pp.model.upload.models import uploads
from pp.model.user.verifycode import VerifyCode
from pp.model.user.weixin import weixin
from pp.model.user.follow import is_following
from pp.model.user.blacklist import in_blacklist
from pp.model.user.loc import user_distance
from pp.model.location.models import locations, Location


@swagger.model
class UserModel(object):
    resource_fields = {
        'id': fields.Integer,
        'gender': fields.String(attribute='gender_str'),
        'favloc': LocationField(attribute='favloc_id'),
        'nickname': fields.String(attribute='showed_nickname'),
        'avatar': StaticField(),
        'following': fields.Boolean(),
        'followed': fields.Boolean(),
        'in_blacklist': fields.Boolean(),
        'dist': UserDistField(),
    }


@swagger.model
class ProfileModel(object):
    resource_fields = dict(UserModel.resource_fields)
    resource_fields.update({
        'phone': fields.String,
    })


@swagger.model
class AvatarModel(object):
    resource_fields = {
        'uri': fields.String(attribute='avatar'),
    }


@swagger.model
class StatModel(object):
    resource_fields = {
        'fav_cinema_count': fields.Integer,
        'ticket_count': fields.Integer,
    }


class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(13), unique=True, index=True)
    password = db.Column(db.String(80))
    openid = db.Column(db.String(28), unique=True, index=True)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

accounts = create_service(Account)


class WeixinAccount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(32), nullable=False)
    openid = db.Column(db.String(28), nullable=False, unique=True, index=True)
    unionid = db.Column(db.String(28), nullable=False)
    access_token = db.Column(db.String(150), nullable=False)
    refresh_token = db.Column(db.String(150), nullable=False)
    scope = db.Column(db.String(40), nullable=False)
    expired_at = db.Column(db.DateTime, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    def refresh_access_token(self):
        try:
            r = weixin.get(
                '/sns/oauth2/refresh_token',
                token=(self.access_token,),
                data={
                    'refresh_token': self.refresh_token,
                    'grant_type': 'refresh_token',
                    'appid': weixin.consumer_key,
                },
            )
        except Exception as e:
            return
        d = r.data
        d['expired_at'] = datetime.now() + timedelta(seconds=d.pop('expires_in'))
        self.orm.update(self, **d)

weixinaccounts = create_service(WeixinAccount)

MALE_GENDER = 'M'
FEMALE_GENDER = 'F'
UNSET_GENDER = 'U'


class GenderType(object):
    unset = 0
    male = 1
    female = 2


class User(CacheableMixin, db.Model, UserMixin):
    cache_region = create_redis_region(MONTH)

    id = db.Column(db.Integer, primary_key=True)  # keep same with Account.id
    gender = db.Column(TINYINT, default=GenderType.unset, nullable=False)
    favloc_id = db.Column(db.Integer, default=0, nullable=False)
    nickname = db.Column(db.String(15), default='', nullable=False)
    avatar_id = db.Column(db.Integer, default=0, nullable=False)
    weixin_avatar = db.Column(db.String(120), default='', nullable=False)  # TODO: drop this field

    # favloc = db.relationship(Location, primaryjoin='Location.id == foreign(User.favloc_id)')

    @cached_property
    def favloc(self):
        return locations.get(self.favloc_id)

    __table_args__ = {'mysql_charset': 'utf8mb4'}

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def following(self):
        me = request and request.user
        if me:
            return is_following(me.id, self.id)
        return False

    @cached_property
    def followed(self):
        me = request and request.user
        if me:
            return is_following(self.id, me.id)
        return False

    @cached_property
    def in_blacklist(self):
        me = request and request.user
        if me:
            return in_blacklist(me.id, self.id)
        return False

    @cached_property
    def gender_str(self):
        return {
            GenderType.unset: 'U',
            GenderType.male: 'M',
            GenderType.female: 'F',
        }[self.gender]

    @cached_property
    def gender_human_str(self):
        return {
            GenderType.unset: u'',
            GenderType.male: u'男',
            GenderType.female: u'女',
        }[self.gender]

    @cached_property
    def account(self):
        return accounts.get(self.id)

    @cached_property
    def phone(self):
        return self.account.phone

    @cached_property
    def avatar(self):
        if self.avatar_id:
            av = uploads.get(self.avatar_id)
            return av.cdn_url()
        return self.weixin_avatar

    @cached_property
    def showed_nickname(self):
        return self.nickname or u'观众'

    @property
    def dist(self):
        if not request or not request.user:
            return ''
        return user_distance(request.user.id, self.id) or ''

    def update_weixin_info(self):
        wa = weixinaccounts.get(self.id)
        if wa.expired_at < datetime.now():
            applog('accounts', 'update_weixin_info err=expired')
            return
        r = weixin.get('/sns/userinfo', token=(wa.access_token,), data={
            'access_token': wa.access_token,
            'openid': wa.openid,
        })
        applog('accounts', 'update_weixin_info id=%s data=%s', self.id, r.data)
        self.gender = r.data['sex']
        self.weixin_avatar = r.data['headimgurl']
        self.nickname = r.data['nickname']
        self.orm.save(self)


users = create_service(User)


def phone_register(phone, plain_password, verify_code):
    if not VerifyCode.verify(phone, verify_code):
        raise exc.WrongVerifyCodeError
    account = accounts.new(
        phone=phone,
        password=generate_password_hash(plain_password),
    )
    try:
        db.session.add(account)
        db.session.flush()
    except IntegrityError:
        db.session.rollback()
        if phone in ('18001290390', '15810097663'):
            return
        raise exc.PhoneRegisteredError
    db.session.add(users.new(id=account.id, nickname=u'观众%s' % str(phone)[-4:]))
    db.session.commit()


def phone_change_password(phone, old_plain_password, new_plain_password):
    account = accounts.first(phone=phone)
    if not account:
        raise exc.AccountNotExistError
    if not check_password_hash(account.password, old_plain_password):
        raise exc.WrongCredentialError
    account.password = generate_password_hash(new_plain_password)
    account.orm.save(account)


def phone_reset_password(phone, plain_password, verify_code):
    if not VerifyCode.verify(phone, verify_code):
        raise exc.WrongVerifyCodeError
    account = accounts.first(phone=phone)
    if not account:
        raise exc.AccountNotExistError
    account.password = generate_password_hash(plain_password)
    account.orm.save(account)


def phone_login(phone, plain_password):
    account = accounts.first(phone=phone)
    if not account:
        raise exc.AccountNotExistError
    if not check_password_hash(account.password, plain_password):
        raise exc.WrongCredentialError
    return users.get(account.id)


def fix_code_to_request(grant_code):
    # walkaround
    args_type = type(request.args)
    req_args = dict(request.args)
    req_args['code'] = grant_code
    request.args = args_type(req_args)


def existed_weixin_login(account, data):
    wa = weixinaccounts.get(account.id)
    wa.orm.update(wa, **data)
    u = users.get(account.id)
    applog('accounts', 'debug id=%s weixin_avatar=%s', u.id, u.weixin_avatar)
    if not u.weixin_avatar:
        u.update_weixin_info()
        u = users.get(u.id)
    applog('accounts', 'debug id=%s weixin_avatar=%s', u.id, u.weixin_avatar)
    return u


def weixin_login(grant_code):
    fix_code_to_request(grant_code)
    try:
        d = weixin.authorized_response()
    except Exception as e:
        applog('accounts', 'weixin: grant_code=%s,err=%s', grant_code, e)
        raise
    applog('accounts', 'weixin: grant_code=%s,d=%s', grant_code, d)
    if d.get(u'errcode'):
        raise exc.WeixinNotAvailableError
    d['expired_at'] = datetime.now() + timedelta(seconds=d.pop('expires_in'))
    d['code'] = grant_code
    a = accounts.first(openid=d['openid'])
    if a:
        return existed_weixin_login(a, d)
    a = accounts.new(
        openid=d['openid'],
    )
    try:
        db.session.add(a)
        db.session.flush()
    except IntegrityError:
        db.session.rollback()
        a = accounts.first(openid=d['openid'])
        return existed_weixin_login(a, d)
    db.session.add(weixinaccounts.new(id=a.id, **d))
    db.session.add(users.new(id=a.id))
    db.session.commit()
    u = users.get(a.id)
    applog('accounts', 'debug id=%s weixin_avatar=%s', u.id, u.weixin_avatar)
    if not u.weixin_avatar:
        u.update_weixin_info()
        u = users.get(u.id)
    applog('accounts', 'debug id=%s weixin_avatar=%s', u.id, u.weixin_avatar)
    return u
