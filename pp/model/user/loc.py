# coding=utf-8

import decimal
from datetime import datetime

from pp.lib.sqlstore import db, create_service
from pp.lib.logger import applog
from pp.lib.loc import get_distance_between_coords


class UserLoc(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=True, index=True, nullable=False)
    lat = db.Column(db.Float, default=0.0, nullable=False)
    lng = db.Column(db.Float, default=0.0, nullable=False)
    updated_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    def update_lat_lng(self, lat, lng):
        oldlat = decimal.Decimal('%.4f' % self.lat)
        oldlng = decimal.Decimal('%.4f' % self.lng)
        newlat = decimal.Decimal('%.4f' % lat)
        newlng = decimal.Decimal('%.4f' % lng)
        if oldlat == newlat and oldlng == newlng:
            return
        self.orm.update(self, lat=lat, lng=lng)

userlocs = create_service(UserLoc)


def set_user_recent_lat_lng(user_id, lat, lng):
    applog('userlocs', 'user_id=%s lat=%s lng=%s', user_id, lat, lng)
    ul = userlocs.first(user_id=user_id)
    if ul:
        ul.update_lat_lng(lat, lng)
    else:
        ul = userlocs.new(user_id=user_id, lat=lat, lng=lng)
        db.session.add(ul)
        db.session.commit()


def user_distance(user1, user2):
    ul1 = userlocs.first(user_id=user1.id)
    ul2 = userlocs.first(user_id=user2.id)
    if not ul1 or not ul2:
        return None
    if any(v == 0.0 for v in [ul1.lat, ul1.lng, ul2.lat, ul2.lng]):
        return None
    return get_distance_between_coords((ul1.lat, ul1.lng), (ul2.lat, ul2.lng))
