# coding=utf-8

from datetime import datetime

from werkzeug.utils import cached_property

import pp.lib.exc as exc
from pp.lib.sqlstore import db, create_service, TINYINT
from pp.lib.logger import applog
from pp.lib.qiniu import cdn_storage


class UploadCate(object):
    avatar = 1
    poster = 2

CATE2NAME = {
    UploadCate.avatar: 'avatar',
    UploadCate.poster: 'poster',
}


class Upload(db.Model):
    __tablename__ = 'avatar'

    id = db.Column(db.Integer, primary_key=True)
    # cate = db.Column(TINYINT, nullable=False)
    # created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)

    VERSION = 2

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    @cached_property
    def cate(self):
        return UploadCate.avatar

    @cached_property
    def catename(self):
        return CATE2NAME[self.cate]

    @cached_property
    def filename(self):
        return '/%s%d/%d' % (self.catename, self.VERSION, self.id)

    @classmethod
    def upload(cls, fileobj):
        up = cls.orm.create()
        cdn_storage.save(fileobj, up.filename)
        return up.id

    def cdn_url(self):
        return cdn_storage.url('/@%s' % self.filename)

uploads = create_service(Upload)
