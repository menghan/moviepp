# coding=utf-8

import time
from datetime import datetime

from flask import current_app

from pp.lib.sqlstore import db, create_service, TINYINT
from pp.lib.logger import applog
from pp.lib.mq import make_async


class SMSState(object):
    created = 0
    sent = 1


class SMS(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(13), nullable=False)
    content = db.Column(db.String(100), nullable=False)
    state = db.Column(TINYINT, default=SMSState.created, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now, nullable=False)
    sent_at = db.Column(db.DateTime, default=0, nullable=False)

    __table_args__ = {'mysql_charset': 'utf8'}

    def __repr__(self):
        return '%s<id=%d>' % (self.__class__.__name__, self.id)

    def send(self):
        emaysms_send.async()(self.id)

    def set_sent(self):
        self.state = SMSState.sent
        self.sent_at = datetime.now()
        self.orm.save(self)

    def has_sent(self):
        return self.state == SMSState.sent

smss = create_service(SMS)


def send(phone, msg):
    # 100 号段用作测试账户
    if str(phone).startswith('100'):
        phone = '13910668848'
    if not isinstance(msg, unicode):
        msg = unicode(msg, 'utf8')
    sender = current_app.config['SMS_SEND_SUFFIX']
    content = u'【%s】%s' % (sender, msg)
    ins = smss.create(phone=phone, content=content)
    applog('sms', 'sms id=%d created at %s', ins.id, datetime.now())
    ins.send()


@make_async()
def emaysms_send(smsid):
    sms = smss.get(smsid)
    if not sms:
        applog('sms', 'sms id=%d not found, sms=%r at %s', smsid, sms, datetime.now())
        return
    if sms.has_sent():
        applog('sms', 'sms id=%d has been sent', smsid)
        return
    if not current_app.config['SMS_ENABLED']:
        applog('sms', 'send sms id=%d success[disable mode]', sms.id)
        sms.set_sent()
        return
    from emaysms import EmaySMS
    emaysms = EmaySMS(None, None)

    emaysms.init(current_app.config['EMAYSMS_SERIALNO'], current_app.config['EMAYSMS_PASSWORD'])
    try:
        emaysms.register()
        resp = emaysms.send([sms.phone], sms.content)
    except Exception as e:
        applog('sms', 'send sms id=%d failed: %s', smsid, e)
        raise
    sms.set_sent()
    applog('sms', 'send sms id=%d success, resp=%s at %s', smsid, resp, datetime.now())
