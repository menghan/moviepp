from flask import Blueprint
from pp.lib.helpers import route

bp = Blueprint(__name__, __name__)


@route(bp, '/')
def index():
    return 'Hello, world!'
