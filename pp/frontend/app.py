import os
from pp.factory import create_app as create_app_base
from pp.lib.helpers import register_blueprints


def create_app(settings_override=None):
    package_name = __name__.rsplit('.', 1)[0]  # rstrip the '.app'
    app = create_app_base(package_name, settings_override)
    register_blueprints(app, package_name, os.path.dirname(os.path.abspath(__file__)))
    return app
