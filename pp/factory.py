# -*- coding: utf-8 -*-

import os
import logging

from flask import Flask
from raven.contrib.flask import Sentry

from pp.lib.exc import AppException
from pp.lib.sqlstore import db
from pp.lib.caching import reset_session_cache


def create_app(package_name, settings_override=None):
    """Returns a :class:`Flask` application instance configured with common
    functionality for the platform.

    :param package_name: application package name
    :param settings_override: a dictionary of settings to override
    """
    app = Flask(package_name)

    # lookup for settings from the package root
    root_name = __name__.split('.', 1)[0]
    app.config.from_object('%s.settings' % root_name)

    custom_settings = os.environ.get('%s_SETTINGS_FILE' % root_name.upper())
    if custom_settings and os.path.exists(custom_settings):
        # some hack
        app.config.root_path = os.path.dirname(custom_settings)
        app.config.from_pyfile(os.path.basename(custom_settings))
    app.config.from_object(settings_override)

    if app.debug:
        app.config['SQLALCHEMY_ECHO'] = True
    db.init_app(app)
    db.app = app
    db.engine.pool._use_threadlocal = True

    init_sentry(app)

    app.before_request(reset_session_cache)
    return app


def init_sentry(app):
    if not app.config.get('SENTRY_DSN'):
        return

    sentry = Sentry()
    app.config['RAVEN_IGNORE_EXCEPTIONS'] = [AppException]
    sentry.init_app(app,
                    dsn=app.config['SENTRY_DSN'],
                    wrap_wsgi=False,
                    register_signal=True)
    app.sentry = sentry
