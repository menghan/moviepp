import os
from pp.factory import create_app as create_app_base
from pp.lib.helpers import register_blueprints
from pp.internal.oauth2 import oauth2_provider


def create_app(settings_override=None):
    package_name = __name__.rsplit('.', 1)[0]  # rstrip the '.app'
    app = create_app_base(package_name, settings_override)
    oauth2_provider.init_app(app)
    oauth2_provider.server
    register_blueprints(app, package_name, os.path.dirname(os.path.abspath(__file__)))
    return app
