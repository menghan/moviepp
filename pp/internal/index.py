from flask import Blueprint, request, current_app

from pp.lib.helpers import route
from pp.internal.oauth2 import oauth2_provider

bp = Blueprint(__name__, __name__)


@route(bp, '/auth/<string:access_token>')
def auth(access_token):
    # TODO: better implement
    validator = oauth2_provider._validator.validate_bearer_token
    ret = validator(access_token, current_app.config['OFFICIAL_SCOPES'], request)
    return str(request.user.id) if ret else 'err'
