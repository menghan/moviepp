from flask_oauthlib.provider import OAuth2Provider
from pp.model.oauth2.models import init_oauth2_provider

oauth2_provider = OAuth2Provider()
init_oauth2_provider(oauth2_provider)
