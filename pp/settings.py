# coding=utf-8

import os

DEBUG = bool(os.environ.get('DEBUG'))

# csrf
CSRF_ENABLED = True
SECRET_KEY = 'secret-key'

# database
DEFAULT_DATABASE_URI = 'sqlite:///' + os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), 'app.db')
SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI', DEFAULT_DATABASE_URI)
SQLALCHEMY_RECORD_QUERIES = True
SQLALCHEMY_POOL_RECYCLE = 3600
DATABASE_QUERY_TIMEOUT = 3

# exception
MAKO_TRANSLATE_EXCEPTIONS = False  # show origin mako render exception
# PROPAGATE_EXCEPTIONS = True

# api client
# FIXME: replace this with generated one
# TODO: support multiple client/scope
OFFICIAL_CLIENT_ID = '6V7ByHDYiujPr64je75qg2DdKNtlLHgj68Mx0HYX'
OFFICIAL_CLIENT_SECRET = 'hNnhFWDghWFyiX5zdlktc8Z7f6oeKNQEQJKbzqcekuproyYAFx'
OFFICIAL_IOS_CLIENT_ID = 'vHnYs1Vv0Y5BqZiEQFftHckjICWpae6IErA9X3H8'
OFFICIAL_IOS_CLIENT_SECRET = 'oiufJIvV29tmRIbUYHgC1aUgWfeioyBqK9RNLJRiFnNVxhM4kB'
OFFICIAL_ANDROID_CLIENT_ID = '6cvMPnx8VE8CMPo7LSZwwdsIgydPuH3zb474w74t'
OFFICIAL_ANDROID_CLIENT_SECRET = 'ihg8kkpP2QhIZ85FKDmNl3yLVTQra4NAJdPcNo8XBpj6whhht0'
OFFICIAL_SCOPES = 'official'.split(' ')

OAUTH2_PROVIDER_TOKEN_EXPIRES_IN = 86400 * 365  # 1year

# log
APPLOG_FILE_ROOT = '.'

# sms
EMAYSMS_SERIALNO = ''
EMAYSMS_PASSWORD = ''
SMS_SEND_SUFFIX = u'电影加加'
SMS_ENABLED = False

CACHE_REDIS_URI = 'redis://127.0.0.1:6379/2'

# flask_restful
ERROR_404_HELP = False  # don't hint on 404 error
RESTFUL_JSON = {
    'indent': None,
    'sort_keys': False,
}

MQTT_REDIS_URI = 'redis://127.0.0.1:6379'

# beanstalk
BEANSTALKD = {
    'host': 'localhost',
    'port': 11300,
}

WEIXIN_CONSUMER_KEY = ''
WEIXIN_CONSUMER_SECRET = ''

STORAGE_NAME = 'avatar_qiniu'
STORAGE_TYPE = 'qiniu'
STORAGE_CONFIG = {
    'access_key': '',
    'secret_key': '',
    'bucket': '',
    'base_url': '',
    'base_dir': '',
}

JPUSH_APIKEY = ''
JPUSH_SECRET = ''
