# coding=utf-8

import sys
import cPickle as pickle
import json

import beanstalkc
from flask import current_app

from pp.lib.logger import applogger
from pp.lib.sqlstore import db

logger = applogger('mq')

MQ_DEFAULT_PRIORITY = 2 ** 31
MQ_DEFAULT_TTR = 120


def create_beanstalk_connection():
    return beanstalkc.Connection(**current_app.config['BEANSTALKD'])


def get_mod_name(func):
    fname = func.__name__
    mod = sys.modules[func.__module__]
    if fname in mod.__dict__:
        raise Exception('ambiguous name %s for function %s' % (fname, func.__name__))
    mod.__dict__[fname] = func
    if mod.__name__ == '__main__':
        mod_name = mod.__file__.split('.')[-2]
    else:
        mod_name = mod.__name__
    return mod_name


def make_async(queue='default'):

    def deco(func):
        mod_name = get_mod_name(func)

        def async(priority=MQ_DEFAULT_PRIORITY, delay=0, ttr=MQ_DEFAULT_TTR):

            def _(*a, **kw):
                from pool.pool import thread_safe_factory
                mq = thread_safe_factory(reset_method=None)(create_beanstalk_connection)()
                task = mod_name, func.__name__, a, kw
                try:
                    mq.use(queue)
                    mq.put(pickle.dumps(task, pickle.HIGHEST_PROTOCOL), priority=priority, delay=delay, ttr=ttr)
                except Exception as e:
                    logger.error('event=put_exception exc=%s task=%r', str(e), task)
                    func(*a, **kw)
            return _

        func.async = async
        return func
    return deco


def async_job_handler(job):
    try:
        try:
            task = pickle.loads(job.body)
        except pickle.UnpicklingError:
            task = json.loads(job.body)
            task = 'pp.model.chat.models', task['handler'], task.get('args', ()), task.get('kwargs', {})
        mod_name, fname, a, kw = task[:4]
        if mod_name not in sys.modules:
            __import__(mod_name)
        module = sys.modules[mod_name]
        func = module.__dict__[fname]

        func(*a, **kw)
    finally:
        db.session.remove()
        job.delete()
