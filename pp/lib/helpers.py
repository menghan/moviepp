import pkgutil
import importlib
from functools import wraps

from flask import Blueprint


def route(bp, *args, **kwargs):
    def decorator(f):
        @bp.route(*args, **kwargs)
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        return f

    return decorator


def register_blueprints(app, package_name, package_path):
    """Register all Blueprint instances on the specified Flask application found
    in all modules for the specified package.

    :param app: the Flask application
    :param package_name: the package name
    :param package_path: the package path
    """
    if isinstance(package_path, basestring):
        package_path = [package_path]
    for _, name, _ in pkgutil.iter_modules(package_path, prefix=package_name + '.'):
        m = importlib.import_module(name)
        for item in dir(m):
            item = getattr(m, item)
            if isinstance(item, Blueprint):
                app.register_blueprint(item)


class LazyCreator(object):

    def __init__(self, factory, *a, **kw):
        def creator():
            return factory(*a, **kw)
        self.creator = creator
        self.__obj = None

    def __getattr__(self, attr):
        if self.__obj is None:
            self.__obj = self.creator()
        return getattr(self.__obj, attr)
