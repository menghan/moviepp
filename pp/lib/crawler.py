# coding=utf-8

from datetime import datetime, timedelta
import json
import time

import requests
from requests.exceptions import ConnectionError, ReadTimeout
from pyquery import PyQuery as pq

from pp.lib.mq import make_async
from pp.lib.logger import applogger
from pp.lib.sqlstore import db
from pp.model.show.models import movies, cinemas, shows

interval = 15
request_kw = {
    'headers': {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
        'referer': 'http://www.meituan.com/',
    },
    'timeout': (5, 30),
}

logger = applogger('spider')


def capture(url):
    logger.info('action=capturing url=%s', url)
    for i in xrange(3):
        try:
            r = requests.get(url, **request_kw)
            if not r.ok:
                logger.error('action=capturing url=%s status_code=%s', url, r.status_code)
                time.sleep(interval)
                continue
            if u'过于频繁' in r.text:
                logger.error('action=capturing url=%s error=need_verify_code', url)
                time.sleep(interval)
                continue
            return pq(r.text)
        except (ConnectionError, ReadTimeout) as e:
            logger.warn('action=capturing error=%s', e)
            time.sleep(interval)
            continue


@make_async('crawl')
def crawl_meituan_onshow():
    db.engine.echo = False
    d = capture('http://bj.meituan.com/dianying/zuixindianying')
    for movie in d('.movie-cell'):
        basic = pq(movie)('.movie-cell__cover')[0]
        name = basic.get('title')
        id = int(basic.get('href').rsplit('/')[-1])
        img = basic.find('img')
        cover = img.get('data-src', img.get('src'))
        span = basic.find('span')
        rating_sec = pq(movie)('.rate-stars')[0]
        rating = int(rating_sec.get('style', '')[6:-1])
        m = movies.first(mt_id=id)
        if not m:
            movies.create(
                name=name,
                mt_id=id,
                mt_cover=cover,
                en_name='',  # TODO
                rating=rating,
            )
        else:
            m.mt_cover = cover
            m.name = name
            m.rating = rating
            m.orm.save(m)
    time.sleep(interval)


def save_movie(movieinfo):
    name = movieinfo('.movie-info__name')[0].get('title', '')
    assert name
    rating_sec = pq(movie)('.rate-stars')[0]
    rating = int(rating_sec.get('style', '')[6:-1])
    logger.critical('unknown_movie_id=%d cinema_id=%d mt_url=http://www.meituan.com/shop/%s',
                    movie_id, cinema_id, cinema.mt_id)


@make_async('crawl')
def crawl_meituan_schedules(cinema_id, d=None, sleep=True):
    db.engine.echo = False
    cinema = cinemas.get(cinema_id)
    d = d or capture('http://%s.meituan.com/shop/%s' % (cinema.loc.mt_abbv, cinema.mt_id))
    cinema_info = json.loads(d('#map-canvas').attr('data-params') or '{}').get('shops', {}).get(str(cinema.mt_id), {})
    if 'phone' in cinema_info and cinema.tel != cinema_info['phone']:
        cinema.tel = cinema_info['phone']
        cinema.orm.save(cinema)
    if 'position' in cinema_info:
        lat, lng = cinema_info['position']
        if lat != cinema.lat or lng != cinema.lng:
            cinema.lat = lat
            cinema.lng = lng
            cinema.orm.save(cinema)
    if 'address' in cinema_info:
        if cinema_info['address'] != cinema.addr:
            cinema.addr = cinema_info['address']
            cinema.orm.save(cinema)
    n_movies, n_schedules = 0, 0
    for movie_info in d('.movie-info'):
        mi = pq(movie_info)
        movie_id = int(mi('.movie-info__name')[0].get('href', '').rsplit('/')[-1])
        movie = movies.first(mt_id=movie_id)
        if not movie:
            # movie = save_movie(mi)
            if not movie:
                continue

        details = list(mi('.movie-info__list dl').items('dt,dd'))
        attrs = []
        for i, detail in enumerate(details):
            if i % 2 != 0:
                continue
            if detail.text() == u'首映：':
                movie.pub_date = datetime.strptime(details[i + 1].text(), '%Y-%m-%d').date()
                continue
            key = detail.text().rstrip(u'：').replace(u' ', u'')
            value = details[i + 1].text().replace(u' ', u'')
            if key == u'主演':
                value = details[i + 1].text().replace(u'.', u'').replace(u' ', u'').rstrip(u'展开')
            attrs.append((key, value))

        newattrs = u' '.join([u'%s:%s' % (k, v) for k, v in attrs])
        if newattrs != movie.attrs:
            movie.attrs = newattrs
            movie.orm.save(movie)

        if not movie.intro:
            time.sleep(interval)
            movie.intro = capture('http://www.meituan.com/movie/%s?mtt=1.movie/movies.0.0.ii1ls4rq' % movie.mt_id)('#J-long-movie-desc').text().rstrip(u'收起')
            movie.orm.save(movie)
        dates = [date.get('data-date') for date in mi('a.show-time-tag')]
        for i, date_schedule in enumerate(mi('.time-table')):
            for schedule in pq(date_schedule)('tr'):
                tds = pq(schedule)('td')
                if not tds or len(tds) == 1:
                    continue
                start_time = pq(tds[0])('.start-time').text()
                hall = tds[2].text
                if hall == u'暂无信息':
                    hall = u'暂时未知'
                shows.create_ignore(
                    cinema_id=cinema.id,
                    movie_id=movie.id,
                    date=dates[i],
                    time='%s %s:00' % (dates[i], start_time),
                    hall=hall,
                )
                n_schedules += 1
            n_movies += 1
    if sleep:
        time.sleep(interval)
    logger.info('import %d movies %d schedules for cinema_id=%d cinema_name=%s', n_movies, n_schedules, cinema.id, cinema.name)
