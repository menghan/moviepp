#!/usr/bin/env python
# coding=utf-8

import os
from werkzeug.contrib.profiler import ProfilerMiddleware as OriginProfilerMiddleware


class ProfilerMiddleware(OriginProfilerMiddleware):

    def __call__(self, environ, start_response):
        if os.environ.get('DEBUG') or '_pp_profile=1' in environ.get('QUERY_STRING', ''):
            return OriginProfilerMiddleware.__call__(self, environ, start_response)

        return self._app(environ, start_response)
