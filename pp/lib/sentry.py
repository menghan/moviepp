# coding=utf-8

from flask import current_app


def get_sentry():
    return getattr(current_app, 'sentry', None)
