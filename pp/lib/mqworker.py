#!/usr/bin/env python
# coding=utf-8

from gevent import monkey
monkey.patch_all()

import logging
import multiprocessing

import gevent
import click

from pp.lib.sentry import get_sentry
from pp.lib.mq import async_job_handler, create_beanstalk_connection, logger as mq_applogger


class MQWorker(gevent.Greenlet):

    def __init__(self, app=None, tube='default', max_jobs=1000):
        gevent.Greenlet.__init__(self)
        self.alive = True
        self.app = app
        self.tube = tube
        self.logger = logging.getLogger(str(self))
        self.jobs = 0
        self.max_jobs = max_jobs

    def __str__(self):
        return '%s-%s' % (self.__class__.__name__, id(self))

    def _run(self):
        try:
            with self.app.app_context():
                from pool.pool import thread_safe_factory
                self.mq = thread_safe_factory(reset_method=None)(create_beanstalk_connection)()
                if self.tube != 'default':
                    self.mq.watch(self.tube)
                    self.mq.ignore('default')

                while self.alive and self.jobs < self.max_jobs:
                    job = self.mq.reserve(timeout=3)
                    if not job:
                        continue
                    with self.app.test_request_context():
                        self.app.preprocess_request()
                        self.handle(job)
                    self.jobs += 1
        except Exception as e:
            sentry = get_sentry()
            sentry and sentry.client and sentry.captureException()
            self.logger.exception('[%s] Uncaught exception: %s', self, e)

    def handle(self, job):
        sentry = get_sentry()
        sentry and sentry.client and sentry.client.context.merge({
            'job': {
                'tube': self.tube,
                'body': job.body.encode('base64'),
            },
        })
        try:
            async_job_handler(job)
        except Exception as e:
            sentry and sentry.client and sentry.captureException()
            self.logger.exception('[%s] Uncaught exception: %s', self, e)
            mq_applogger.exception('event=handle_exception exc=%s body=%s', str(e), job.body.encode('base64'))
        finally:
            sentry and sentry.client and sentry.client.context.clear()


@click.command()
@click.option('--queue', default='default', help='job queue')
@click.option('--process', default=1, type=click.IntRange(1, multiprocessing.cpu_count() * 2), help='process count')
@click.option('--thread', default=5, type=click.IntRange(1, 10), help='greenlet count')
@click.option('--max_jobs', default=1000, type=click.IntRange(1, 1e4), help='max job before quit')
@click.option('--name', default='', help='addition name on proctitle')
def main(queue, process, thread, max_jobs, name):
    from pistil.pool import PoolArbiter
    from pistil.gevent_worker import GreenletsArbiterWorker
    from pp.app import flask_app as app
    PoolArbiter(
        {
            'num_workers': process,
            'num_greenlets': thread,
            'greenlet_cls': MQWorker,
            'cls_kwargs': {
                'app': app,
                'tube': queue,
                'max_jobs': max_jobs,
            },
        },
        (
            GreenletsArbiterWorker,
            30,
            'worker',
            {},
            'mq: %s%s' % (queue, ' - ' + name if name else ''),
        ),
    ).run()
