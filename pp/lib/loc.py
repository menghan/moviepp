# coding=utf-8

import sys
import math


def get_distance_between_coords(c1, c2):
    """Returns the distance in meters between two coordinates"""

    # normalize the coords to six decimal place
    if '%.6f' % c1[0] == '%.6f' % c2[0] and '%.6f' % c1[1] == '%.6f' % c2[1]:
        return 0.0

    EARTH_RADIUS = 3963.19
    METERS = 1609.344
    TORADIANS = 57.2957795
    lat1 = c1[0] / TORADIANS
    lat2 = c2[0] / TORADIANS
    lng1 = c1[1] / TORADIANS
    lng2 = c2[1] / TORADIANS
    tmp = (math.sin(lat1) * math.sin(lat2) +
           math.cos(lat1) * math.cos(lat2) * math.cos(lng2 - lng1))
    dist = EARTH_RADIUS * math.acos(tmp) * METERS
    return dist


def get_nearest(lat_lng, positions):
    min_dis = sys.maxint  # long enough
    min_pos = None
    lat, lng = lat_lng
    for pos in positions:
        if pos.lat == 0.0 or pos.lng == 0.0:
            continue
        dis = get_distance_between_coords((lat, lng), (pos.lat, pos.lng))
        if dis < min_dis:
            min_dis = dis
            min_pos = pos
    return min_pos, min_dis


def distance_summary(dist):
    if dist < 100:
        return u'附近'
    elif dist < 1000:
        return u'%d00米' % (dist / 100)
    elif dist < 10000:
        return u'%.1f公里' % (dist / 1000.0)
    else:
        return u'%d公里' % (dist / 1000)
