# coding=utf-8

from functools import wraps


class Lazy(object):

    __slots__ = ('__creator', '__wrapped', '__evaluated', '__lazy_group', '__dict__')

    def __init__(self, lazy_group, func, *a, **kw):
        def creator():
            return func(*a, **kw)
        self.__creator = creator
        self.__wrapped = None
        self.__evaluated = False
        self.__lazy_group = lazy_group

    def _evaluate_wrapped_lazy(self):
        if not self.__evaluated:
            self.__wrapped = self.__creator()
            self.__evaluated = True

    def _get_wrapped(self):
        if not self.__evaluated:
            self.__lazy_group.trigger_batch_create()
        return self.__wrapped

    def __getattr__(self, attr):
        wrapped = self._get_wrapped()

        # make flask_restful's marshal happy
        if isinstance(wrapped, dict) and attr in wrapped:
            return wrapped[attr]

        return getattr(wrapped, attr)

    @property
    def __dict__(self):
        try:
            return self._get_wrapped().__dict__
        except RuntimeError:
            raise AttributeError('__dict__')

    def __repr__(self):
        try:
            obj = self._get_wrapped()
        except RuntimeError:
            return '<%s unbound>' % self.__class__.__name__
        return repr(obj)

    def __bool__(self):
        try:
            return bool(self._get_wrapped())
        except RuntimeError:
            return False

    def __unicode__(self):
        try:
            return unicode(self._get_wrapped())  # noqa
        except RuntimeError:
            return repr(self)

    def __dir__(self):
        try:
            return dir(self._get_wrapped())
        except RuntimeError:
            return []

    __getslice__ = lambda x, i, j: x._get_wrapped()[i:j]
    __str__ = lambda x: str(x._get_wrapped())
    __lt__ = lambda x, o: x._get_wrapped() < o
    __le__ = lambda x, o: x._get_wrapped() <= o
    __eq__ = lambda x, o: x._get_wrapped() == o
    __ne__ = lambda x, o: x._get_wrapped() != o
    __gt__ = lambda x, o: x._get_wrapped() > o
    __ge__ = lambda x, o: x._get_wrapped() >= o
    __cmp__ = lambda x, o: cmp(x._get_wrapped(), o)  # noqa
    __hash__ = lambda x: hash(x._get_wrapped())
    __call__ = lambda x, *a, **kw: x._get_wrapped()(*a, **kw)
    __len__ = lambda x: len(x._get_wrapped())
    __getitem__ = lambda x, i: x._get_wrapped()[i]
    __iter__ = lambda x: iter(x._get_wrapped())
    __contains__ = lambda x, i: i in x._get_wrapped()
    __add__ = lambda x, o: x._get_wrapped() + o
    __sub__ = lambda x, o: x._get_wrapped() - o
    __mul__ = lambda x, o: x._get_wrapped() * o
    __floordiv__ = lambda x, o: x._get_wrapped() // o
    __mod__ = lambda x, o: x._get_wrapped() % o
    __divmod__ = lambda x, o: x._get_wrapped().__divmod__(o)
    __pow__ = lambda x, o: x._get_wrapped() ** o
    __lshift__ = lambda x, o: x._get_wrapped() << o
    __rshift__ = lambda x, o: x._get_wrapped() >> o
    __and__ = lambda x, o: x._get_wrapped() & o
    __xor__ = lambda x, o: x._get_wrapped() ^ o
    __or__ = lambda x, o: x._get_wrapped() | o
    __div__ = lambda x, o: x._get_wrapped().__div__(o)
    __truediv__ = lambda x, o: x._get_wrapped().__truediv__(o)
    __neg__ = lambda x: -(x._get_wrapped())
    __pos__ = lambda x: +(x._get_wrapped())
    __abs__ = lambda x: abs(x._get_wrapped())
    __invert__ = lambda x: ~(x._get_wrapped())
    __complex__ = lambda x: complex(x._get_wrapped())
    __int__ = lambda x: int(x._get_wrapped())
    __long__ = lambda x: long(x._get_wrapped())  # noqa
    __float__ = lambda x: float(x._get_wrapped())
    __oct__ = lambda x: oct(x._get_wrapped())
    __hex__ = lambda x: hex(x._get_wrapped())
    __index__ = lambda x: x._get_wrapped().__index__()
    __coerce__ = lambda x, o: x._get_wrapped().__coerce__(x, o)
    __enter__ = lambda x: x._get_wrapped().__enter__()
    __exit__ = lambda x, *a, **kw: x._get_wrapped().__exit__(*a, **kw)
    __radd__ = lambda x, o: o + x._get_wrapped()
    __rsub__ = lambda x, o: o - x._get_wrapped()
    __rmul__ = lambda x, o: o * x._get_wrapped()
    __rdiv__ = lambda x, o: o / x._get_wrapped()
    __rtruediv__ = lambda x, o: x._get_wrapped().__rtruediv__(o)
    __rfloordiv__ = lambda x, o: o // x._get_wrapped()
    __rmod__ = lambda x, o: o % x._get_wrapped()
    __rdivmod__ = lambda x, o: x._get_wrapped().__rdivmod__(o)


class LazyGroup(object):

    def __init__(self, create, pre_create):
        self.create = create
        self.pre_create = pre_create
        self.lazying = []
        self.lazy_akw = []

    def new_lazy(self, *a, **kw):
        lazy = Lazy(self, self.create, *a, **kw)
        self.lazying.append(lazy)
        self.lazy_akw.append((a, kw))
        return lazy

    def trigger_batch_create(self):
        self.pre_create(self.lazy_akw)
        for lazy in self.lazying:
            lazy._evaluate_wrapped_lazy()
        self.lazy_akw = []
        self.lazying = []


def lazy_to_batch(warm_func):
    def wrapper(f):
        group = LazyGroup(f, warm_func)
        @wraps(f)
        def _(*a, **kw):
            return group.new_lazy(*a, **kw)
        return _
    return wrapper
