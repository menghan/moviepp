# coding=utf-8

import os
import logging
import logging.handlers

from flask import current_app

from pp.lib.helpers import LazyCreator

_loggers = {}


def applogger(name):
    if name not in _loggers:
        logger = LazyCreator(_create_applogger, name)
        _loggers[name] = logger

    return _loggers[name]


def _create_applogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    for handler in logger.handlers:
        logger.removeHandler(handler)

    logfile_root = current_app.config['APPLOG_FILE_ROOT']
    logfile = os.path.join(logfile_root, name + '.log')
    handler = logging.handlers.WatchedFileHandler(logfile, delay=True)
    handler.setFormatter(logging.Formatter('time=%(asctime)s pid=%(process)d level=%(levelname)s message=%(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)
    logger.propagate = False
    return logger


def applog(category, *message):
    logger = applogger(category)
    logger.info(*message)
