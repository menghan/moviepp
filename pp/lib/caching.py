# file: caching.py
# Full article: http://www.debrice.com/flask-sqlalchemy-caching/

import functools
import hashlib
import itertools

from flask import current_app, request
#  from pool.pool import thread_safe_factory
from werkzeug.utils import cached_property
from sqlalchemy import event
from sqlalchemy.orm.attributes import get_history
from sqlalchemy.ext.declarative import declared_attr
from dogpile.cache.region import make_region
from dogpile.cache.api import NO_VALUE
from dogpile.cache.backends.memory import MemoryBackend
from dogpile.cache.backends.null import NullBackend
from dogpile.cache.proxy import ProxyBackend as BackendProxy

from pp.lib.sentry import get_sentry

MINUTE = 60
HOUR = MINUTE * 60
DAY = HOUR * 24
WEEK = DAY * 7
MONTH = WEEK * 4


def md5_key_mangler(key):
    """
    Encodes SELECT queries (key) into md5 hashes
    """
    if key.startswith('SELECT '):
        key = hashlib.md5(key.encode('ascii')).hexdigest()
    return key


def memoize(obj):
    """
    Local cache of the function return value
    """
    cache = obj.cache = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer


def create_region(**cfg):
    return make_region(key_mangler=md5_key_mangler).configure(**cfg)


def reset_session_cache():
    request.cache_backend = MemoryBackend(arguments={})


class SessionBackendProxy(BackendProxy):

    def __init__(self):
        BackendProxy.__init__(self)
        self.null_backend = NullBackend(None)

    @property
    def cache_backend(self):
        return getattr(request, 'cache_backend', self.null_backend)

    def get(self, key):
        r = self.cache_backend.get(key)
        if r is NO_VALUE:
            r = self.proxied.get(key)
            if r is not NO_VALUE:
                self.cache_backend.set(key, r)
        return r

    def set(self, key, value):
        self.cache_backend.set(key, value)
        self.proxied.set(key, value)

    def delete(self, key):
        self.cache_backend.delete(key)
        self.proxied.delete(key)

    def get_multi(self, keys):
        objs = self.cache_backend.get_multi(keys)
        nv_keys = []
        nv_keys_to_keyindex = {}
        for i, obj in enumerate(objs):
            if obj is NO_VALUE:
                nv_keys.append(keys[i])
                nv_keys_to_keyindex[keys[i]] = i
        if not nv_keys:
            return objs
        nv_objs = self.proxied.get_multi(nv_keys)
        for i, obj in enumerate(nv_objs):
            if obj is not NO_VALUE:
                keyindex = nv_keys_to_keyindex[nv_keys[i]]
                objs[keyindex] = obj
                self.cache_backend.set(keys[keyindex], obj)
        return objs

    def set_multi(self, mappings):
        self.cache_backend.set_multi(mappings)
        self.proxied.set_multi(mappings)

    def delete_multi(self, keys):
        self.cache_backend.delete_multi(keys)
        self.proxied.delete_multi(keys)

    def get_mutex(self, key):
        return self.proxied.get_mutex(key)


#  @thread_safe_factory(reset_method=None, close_method=None, use_threadlocal=True)
def create_redis_region(expire):
    return create_region(
        backend='dogpile.cache.redis',
        arguments={
            'url': current_app.config['CACHE_REDIS_URI'],
            'redis_expiration_time': expire,
            'distributed_lock': True,
        },
        wrap=[SessionBackendProxy],
    )


def _key_from_query(query, qualifier=None):
    """
    Given a Query, create a cache key.
    """

    stmt = query.with_labels().statement
    compiled = stmt.compile()
    params = compiled.params

    return " ".join(
                    [str(compiled)] +
                    [str(params[k]) for k in sorted(params)])


class Cache(object):

    def __init__(self, model):
        self.model = model
        self.region = model.cache_region
        self.version = getattr(model, 'cache_version', '0')
        self.query_fields = getattr(self.model, 'cache_query_fields', [])

    def get(self, id):
        """
        Equivalent to the Model.query.get(id) but using cache
        """
        cache_key = self._make_cache_key(id)
        obj = self.region.get(cache_key)
        if obj is NO_VALUE:
            return self._flush_get(id)
        else:
            return obj

    def _flush_get(self, id):
        cache_key = self._make_cache_key(id)
        obj = self.model.query.get(id)
        self.region.set(cache_key, obj)
        return obj

    def first(self, order_by='asc', **kwargs):
        objs = self.findall(order_by=order_by, offset=0, limit=1, **kwargs)
        if objs:
            return objs[0]

    def findall(self, order_by='asc', offset=None, limit=None, **kwargs):
        """
        Retrieve all the objects ids then pull them independently from cache.
        kwargs accepts one attribute filter, mainly for relationship pulling.
        offset and limit allow pagination, order by for sorting (asc/desc).
        """
        for key in kwargs:
            if key not in self.columns:
                raise TypeError('%s does not have an attribute %s' % self, key)

        cache_key = self._make_cache_key(**kwargs)
        ids = self.region.get(cache_key)

        if ids is NO_VALUE:
            ids = [o.id for o in self.model.query.filter_by(**kwargs).with_entities(self.model.id)]
            self.region.set(cache_key, ids)

        if order_by == 'desc':
            ids.reverse()

        if offset is not None:
            ids = ids[offset:]

        if limit is not None:
            ids = ids[:limit]
        objs = self.gets(ids)
        try:
            assert all(objs)
        except AssertionError:
            sentry = get_sentry()
            sentry and sentry.client and sentry.captureException()
            objs = filter(None, objs)
        return objs

    def gets(self, ids):
        keys = [self._make_cache_key(id) for id in ids]
        objs = []
        for pos, obj in enumerate(self.region.get_multi(keys)):
            if obj is NO_VALUE:
                objs.append(self._flush_get(ids[pos]))
            else:
                objs.append(obj)
        return objs

    @cached_property
    def columns(self):
        return [c.name for c in self.model.__table__.columns if c.name != 'id']

    @memoize
    def _make_cache_key(self, id="all", **kwargs):
        """
        Generate a key as query
        format: '<tablename>.<column>[<value>]'

        'user.id[all]': all users
        'address.user_id=4[all]': all address linked to user id 4
        'user.id[4]': user with id=4
        """
        q_filter = "".join("%s=%s" % (k, kwargs[k]) for k in sorted(kwargs)) or 'id'
        return "%s%s.%s[%s]" % (self.model.__tablename__, getattr(self.model, 'cache_version', '0'), q_filter, id)

    def flush(self, obj):
        keys = []
        changed = {}
        for column in self.columns:
            added, unchanged, deleted = get_history(obj, column)
            if deleted or added:
                changed[column] = list(deleted) + list(added)
        if obj in obj._sa_instance_state.session.deleted:
            for column in self.columns:
                changed[column] = [getattr(obj, column)]
        for column in changed:
            for value in changed[column]:
                keys.append(self._make_cache_key(**{column: value}))
        for query_fields in self.query_fields:
            comp = itertools.product(*[
                changed.get(field, [getattr(obj, field)])
                for field in query_fields
            ])
            for values in comp:
                d = dict(itertools.izip(query_fields, values))
                keys.append(self._make_cache_key(**d))
        # flush "all" listing
        keys.append(self._make_cache_key())
        # flush the object
        if obj.id is not None:
            keys.append(self._make_cache_key(obj.id))

        self.region.delete_multi(keys)


class CacheableMixin(object):

    @declared_attr
    def cache(self):
        """
        Add the cache features to the model
        """
        return Cache(self)

    @staticmethod
    def __flush_cache(mapper, connection, target):
        """
        Called on object modification to flush cache of dependencies
        """
        target.cache.flush(target)

    @classmethod
    def __declare_last__(cls):
        """
        Auto clean the caches, including listings possibly associated with
        this instance, on delete, update and insert.
        """
        event.listen(cls, 'before_delete', cls.__flush_cache)
        event.listen(cls, 'before_update', cls.__flush_cache)
        event.listen(cls, 'before_insert', cls.__flush_cache)
