# coding=utf-8


class AppException(Exception):
    status = 200


class WrongVerifyCodeError(AppException):
    code = 8000
    description = u'验证码错误'


class PhoneRegisteredError(AppException):
    code = 8001
    description = u'该手机号已注册'


class AccountNotExistError(AppException):
    code = 8002
    description = u'该用户不存在'


class WrongCredentialError(AppException):
    code = 8003
    description = u'密码不正确'


class SendTooFrequentlyError(AppException):
    code = 8004
    description = u'短信请求得太频繁了'


class ShowNotFoundError(AppException):
    code = 8005
    description = u'场次不存在'


class MovieNotFoundError(AppException):
    code = 8006
    description = u'电影不存在'


class UserNotFoundError(AppException):
    code = 8007
    description = u'用户不存在'


class TicketNotFoundError(AppException):
    code = 8008
    description = u'电影票不存在'


class TicketNotOwnedError(AppException):
    code = 8009
    description = u'电影票不属于你'


class InvalidArgumentError(AppException):
    code = 8010
    description = u'API参数不正确'


class NotAddTicketError(AppException):
    code = 8011
    description = u'还没有添加这个场次的电影票'


class NotLoginError(AppException):
    code = 8012
    description = u'用户未登录'


# not used
class DuplicatedTicketInOneShowError(AppException):
    code = 8013
    description = u'这个场次已经添加过票'


class CantMatchShowError(AppException):
    code = 8014
    description = u'没找到对应的场次'


class CityNotSetOrNotFoundError(AppException):
    code = 8015
    description = u'请完善常居地信息'


class WeixinNotAvailableError(AppException):
    code = 8016
    description = u'网络有点问题，请稍候重试'


class InvalidPhoneError(AppException):
    code = 8017
    description = u'手机号不正确'


api_errors = {}
_g = dict(globals())
for _k, _v in _g.iteritems():
    if _k.endswith('Error'):
        api_errors[_k] = {
            'code': _v.code,
            'status': _v.status,
            'description': _v.description,
        }
del _k, _v
