from datetime import datetime
from functools import wraps

from werkzeug.utils import cached_property
from sqlalchemy.exc import IntegrityError
from sqlalchemy.types import BigInteger
from sqlalchemy.dialects.mysql import TINYINT
from flask_sqlalchemy import SQLAlchemy


class CustomSQLAlchemy(SQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        if not 'isolation_level' in options:
            options['isolation_level'] = 'READ COMMITTED'
        return SQLAlchemy.apply_driver_hacks(self, app, info, options)


db = CustomSQLAlchemy()


def assure_in_session(f):
    @wraps(f)
    def _(*a, **kw):
        r = f(*a, **kw)
        for o in r if isinstance(r, (tuple, list)) else [r]:
            if isinstance(o, db.Model) and o not in db.session:
                try:
                    db.session.add(o)
                except AssertionError:
                    pass
        return r
    return _


class Service(object):
    """A :class:`Service` instance encapsulates common SQLAlchemy model
    operations in the context of a :class:`Flask` application.
    """
    __model__ = None

    def __getattr__(self, attr):
        if self.__model__:
            return getattr(self.__model__, attr)
        raise AttributeError("'%s' object has no attribute '%s'" % (self.__class__.__name__, attr))

    def _isinstance(self, model, raise_error=True):
        """Checks if the specified model instance matches the service's model.
        By default this method will raise a `ValueError` if the model is not the
        expected type.

        :param model: the model instance to check
        :param raise_error: flag to raise an error on a mismatch
        """
        rv = isinstance(model, self.__model__)
        if not rv and raise_error:
            raise ValueError('%s is not of type %s' % (model, self.__model__))
        return rv

    def _preprocess_params(self, kwargs):
        """Returns a preprocessed dictionary of parameters. Used by default
        before creating a new instance or updating an existing instance.

        :param kwargs: a dictionary of parameters
        """
        kwargs.pop('csrf_token', None)
        return kwargs

    @property
    def _query(self):
        return self.__model__.query

    @cached_property
    def _cache(self):
        return getattr(self.__model__, 'cache', None)

    def save(self, model):
        """Commits the model to the database and returns the model

        :param model: the model to save
        """
        self._isinstance(model)
        db.session.add(model)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise
        db.session.refresh(model)
        return model

    def all(self):
        """Returns a generator containing all instances of the service's model.
        """
        return self._query.all()

    @assure_in_session
    def get(self, id):
        """Returns an instance of the service's model with the specified id.
        Returns `None` if an instance with the specified id does not exist.

        :param id: the instance id
        """
        if self._cache:
            return self._cache.get(id)
        return self._query.get(id)

    @assure_in_session
    def gets(self, ids):
        """Returns a list of instances of the service's model with the specified
        ids.

        :param *ids: instance ids
        """
        if not ids:
            return []
        if self._cache:
            return self._cache.gets(ids)
        return self._query.filter(self.__model__.id.in_(ids)).all()

    def find(self, **kwargs):
        """Returns a list of instances of the service's model filtered by the
        specified key word arguments.

        :param **kwargs: filter parameters
        """
        return self._query.filter_by(**kwargs)

    @assure_in_session
    def findall(self, **kwargs):
        if self._cache:
            return self._cache.findall(**kwargs)
        return self.find(**kwargs).all()

    @assure_in_session
    def first(self, **kwargs):
        """Returns the first instance found of the service's model filtered by
        the specified key word arguments.

        :param **kwargs: filter parameters
        """
        if self._cache:
            return self._cache.first(**kwargs)
        return self.find(**kwargs).first()

    def new(self, **kwargs):
        """Returns a new, unsaved instance of the service's model class.

        :param **kwargs: instance parameters
        """
        return self.__model__(**self._preprocess_params(kwargs))  # pylint: disable=E

    def create(self, **kwargs):
        """Returns a new, saved instance of the service's model class.

        :param **kwargs: instance parameters
        """
        return self.save(self.new(**kwargs))

    def create_ignore(self, **kwargs):
        """Returns a new, saved instance of the service's model class.
        return existed instance if creation raises IntegrityError

        :param **kwargs: instance parameters
        """
        try:
            return self.create(**kwargs)
        except IntegrityError:
            return self.first(**kwargs)

    def update(self, model, **kwargs):
        """Returns an updated instance of the service's model class.

        :param model: the model to update
        :param **kwargs: update parameters
        """
        self._isinstance(model)
        for k, v in self._preprocess_params(kwargs).items():
            setattr(model, k, v)
        if hasattr(model, 'updated_at'):
            model.updated_at = datetime.now()
        self.save(model)
        return model

    def delete(self, model, commit_transaction=True):
        """Immediately deletes the specified model instance.

        :param model: the model instance to delete
        """
        self._isinstance(model)
        db.session.delete(model)
        if commit_transaction:
            db.session.commit()


def create_service(klass):

    class _(Service):
        __model__ = klass

    s = _()
    klass.orm = s
    return s
