FROM python:2.7-slim

WORKDIR /app
COPY . .

RUN pip install --upgrade pip wheel && \
	pip install --use-wheel -r requirements.txt && \
	python setup.py develop

ENTRYPOINT ["/usr/local/bin/manage"]
