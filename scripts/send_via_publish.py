#!/usr/bin/env python
# coding=utf-8

import sys
import json
import time
from datetime import datetime
from mqttclient import MQTTClient


def main():
    try:
        topic = sys.argv[1]
    except IndexError:
        topic = 'chatroom_1'
    try:
        content = sys.argv[2]
    except IndexError:
        content = '测试内容带"引号的_%s' % datetime.now()
    with MQTTClient(timeout=0.001, username='1') as mc:
        n = 1000
        t = time.time()
        for i in xrange(n + 1):
            content = '%d_%s_avoiding_print' % (i, datetime.now())
            payload = json.dumps({'payload': content})
            mc.publish(topic, payload, qos=1)
        mc.join()
        cost = time.time() - t
        print 'using %.2f seconds to send %d messages(qos=1)' % (cost, n + 1)


if __name__ == '__main__':
    main()
