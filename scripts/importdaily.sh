#!/usr/bin/env bash

. /var/app/moviepp/environ

function get_cinemas()
{
	manage list_meituan_cinemas_sorted --filterhot=1 --filterhasschedule=1
	manage list_meituan_cinemas_sorted --filterhot=0 --filterhasschedule=1
	manage list_meituan_cinemas_sorted --filterhasschedule=0
}

manage fetch_meituan_schedules --mt_id "`get_cinemas`" 2>&1 | tee -a /tmp/crawl.log
