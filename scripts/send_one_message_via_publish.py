#!/usr/bin/env python
# coding=utf-8

import sys
import json
from datetime import datetime
from mqttclient import MQTTClient


def main():
    try:
        topic = sys.argv[1]
    except IndexError:
        topic = 'chatroom_1'
    content = u'测试内容带"引号的带emoji%s的_%s' % (u'\U0001f604', datetime.now())
    with MQTTClient(timeout=0.001, username='1') as mc:
        payload = json.dumps({'payload': content})
        mc.publish(topic, payload, qos=1)
        print 'sent "%s" at %s' % (payload, datetime.now())
        mc.join()


if __name__ == '__main__':
    main()
