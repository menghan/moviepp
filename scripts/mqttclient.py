#!/usr/bin/env python
# coding=utf-8

import json
import sys
import Queue
import time
from datetime import datetime
from mosquitto import Mosquitto, MOSQ_ERR_SUCCESS

DEFAULT_MQTT_HOST = '127.0.0.1'
DEFAULT_MQTT_PORT = 4392


class MQTTClient(object):

    def __init__(self, **kwargs):
        self.attrs = kwargs
        self.Q = None
        self.client = None
        self.sentQ = None

    def __enter__(self):
        self.Q = Queue.Queue()
        self.sentQ = Queue.Queue()
        self.client = Mosquitto(clean_session=True)
        self.client.on_message = self.on_message
        self.client.on_log = self.on_log
        self.client.on_publish = self.on_publish

        username = self.attrs.get('username')
        password = self.attrs.get('password')
        self.client.username_pw_set(username, password)

        timeout = self.attrs.get('timeout', 1.0)
        old_loop_forever_impl = self.client.loop_forever

        def loop_forever(timeout=timeout, max_packets=1):
            return old_loop_forever_impl(timeout, max_packets)
        self.client.loop_forever = loop_forever

        host = self.attrs.get('MQTT_HOST', DEFAULT_MQTT_HOST)
        port = self.attrs.get('MQTT_PORT', DEFAULT_MQTT_PORT)
        keepalive = self.attrs.get('MQTT_KEEPALIVE', 60)
        assert self.client.connect(host, port, keepalive=keepalive) == 0
        self.client.loop_start()
        time.sleep(timeout * 2)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.Q = None
        self.client.disconnect()
        self.client.loop_stop()
        self.client = None
        if any([exc_type, exc_value, traceback]):
            raise (exc_type, exc_value, traceback)

    def publish(self, topic, payload=None, qos=0, retain=False):
        result, mid = self.client.publish(topic, payload, qos, retain)
        assert result == MOSQ_ERR_SUCCESS
        self.sentQ.put(mid)

    def join(self):
        self.sentQ.join()

    def on_log(self, mosq, userdata, level, buf):
        pass

    def on_publish(self, mosq, userdata, mid):
        self.sentQ.get_nowait()
        self.sentQ.task_done()

    def on_message(self, mosq, userdata, message):
        self.Q.put(message)

    def get_message(self, *args, **kwargs):
        return self.Q.get(*args, **kwargs)


def main():
    n = 1000
    t = time.time()
    with MQTTClient(username='2', timeout=0.001) as mc:
        mc.client.subscribe('chatroom_1')
        for i in xrange(n + 1):
            msg = mc.get_message()
            received_at = datetime.now()
            try:
                payload_timestamp = json.loads(msg.payload)['payload'][-len('2015-07-05 23:15:27.695439'):]
                sent_at = datetime.strptime(payload_timestamp, '%Y-%m-%d %H:%M:%S.%f')
            except:
                if '-v' in sys.argv:
                    print 'received "%s" at %s' % (msg.payload, received_at)
            else:
                delta = received_at - sent_at
                print 'latency: %dms received "%s" at %s' % (delta.microseconds / 1000, msg.payload, received_at)
            if i == 0:
                t = time.time()
    cost = time.time() - t
    print 'using %.2f seconds to receive %d messages' % (cost, n)


if __name__ == '__main__':
    main()
