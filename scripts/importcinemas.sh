#!/usr/bin/env bash

. /var/app/moviepp/environ

while read city; do
	echo $city fetching...
	if ! manage fetch_meituan_cinemas --city $city; then
		date
		echo failed to update city $city, breaking...
		break
	fi
	echo $city done
	sleep 15
done
