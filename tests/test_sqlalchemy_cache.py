# coding=utf-8

import time
from mock import patch
from flask.ext.testing import TestCase
from pp.factory import create_app as create_app_factory
from pp.lib.sqlstore import db


class CacheTest(TestCase):

    def create_app(self):
        return create_app_factory(__name__)

    def setUp(self):
        TestCase.setUp(self)
        import pp.model.testmodel.models as models
        self.M = models.TestM
        db.engine.echo = True
        tables = [self.M.metadata.tables['testM']]
        self.M.metadata.create_all(tables=tables, bind=db.engine)

    def tearDown(self):
        TestCase.tearDown(self)
        db.session.remove()
        tables = [self.M.metadata.tables['testM']]
        self.M.metadata.drop_all(tables=tables, bind=db.engine)

    def test_smoke(self):
        base = int(float(time.time()) * 1e6) % 100000000
        # create
        m = self.M.orm.create(cinema_id=base + 1, movie_id=base + 2)
        assert m
        assert m.cinema_id == base + 1
        assert m.movie_id == base + 2

        # get
        m1 = self.M.query.get(m.id)
        assert id(m1) == id(m)

        # filter
        assert self.M.query.filter_by(cinema_id=base + 1, movie_id=base + 2).first().id == m.id
        assert len(self.M.query.all()) == 1

        # update
        m.movie_id = base + 3
        self.M.orm.save(m)
        assert m.movie_id == base + 3
        m2 = self.M.query.get(m.id)
        assert id(m2) == id(m)
        assert self.M.query.filter_by(cinema_id=base + 1, movie_id=base + 2).first() is None
        assert self.M.query.filter_by(cinema_id=base + 1, movie_id=base + 3).first().id == m.id

        # delete
        self.M.orm.delete(m)
        assert self.M.query.filter_by(cinema_id=base + 1, movie_id=base + 3).first() is None
        assert len(self.M.query.all()) == 0

    def test_cache(self):
        base = int(float(time.time()) * 1e6) % 100000000
        # create
        m = self.M.orm.create(cinema_id=base + 1, movie_id=base + 2)
        assert m
        assert m.cinema_id == base + 1
        assert m.movie_id == base + 2

        # get
        m1 = self.M.orm.get(m.id)
        assert m1.id == m.id
        assert m1.cinema_id == base + 1
        assert m1.movie_id == base + 2

        # filter
        assert self.M.orm.first(cinema_id=base + 1).id == m.id
        assert self.M.orm.first(movie_id=base + 2).id == m.id
        assert self.M.orm.first(cinema_id=base + 1, movie_id=base + 2).id == m.id
        assert len(self.M.orm.findall()) == 1

        # can load from cache
        with patch.object(self.M, 'query'):
            assert self.M.orm.first(cinema_id=base + 1).id == m.id
            assert self.M.orm.first(movie_id=base + 2).id == m.id
            assert self.M.orm.first(cinema_id=base + 1, movie_id=base + 2).id == m.id
            assert len(self.M.orm.findall()) == 1

        # update
        m1.movie_id = base + 3
        self.M.orm.save(m1)
        m2 = self.M.orm.get(m1.id)
        assert m2.id == m1.id
        assert self.M.orm.first(cinema_id=base + 1).id == m1.id
        assert self.M.orm.first(movie_id=base + 2) is None
        assert self.M.orm.first(movie_id=base + 3).id == m1.id
        assert self.M.orm.first(cinema_id=base + 1, movie_id=base + 2) is None
        assert self.M.orm.first(cinema_id=base + 1, movie_id=base + 3).id == m1.id
        assert self.M.orm.first(cinema_id=base + 1).id == m1.id

        # delete
        self.M.orm.delete(m2)
        assert self.M.orm.get(m2.id) is None
        assert self.M.orm.first(movie_id=base + 3) is None
        assert self.M.orm.first(cinema_id=base + 1, movie_id=base + 3) is None
        assert self.M.orm.first(cinema_id=base + 1) is None
        assert len(self.M.orm.findall()) == 0
