"""empty message

Revision ID: 96795f8badc
Revises: 23753bb92fff
Create Date: 2015-07-28 22:02:39.613993

"""

# revision identifiers, used by Alembic.
revision = '96795f8badc'
down_revision = '23753bb92fff'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('account', sa.Column('openid', sa.String(length=28), nullable=True))
    op.create_index(op.f('ix_account_openid'), 'account', ['openid'], unique=True)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_account_openid'), table_name='account')
    op.drop_column('account', 'openid')
    ### end Alembic commands ###
