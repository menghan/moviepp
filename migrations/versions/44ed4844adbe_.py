"""empty message

Revision ID: 44ed4844adbe
Revises: 2589fb165f75
Create Date: 2015-08-29 11:50:24.172274

"""

# revision identifiers, used by Alembic.
revision = '44ed4844adbe'
down_revision = '2589fb165f75'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('notification', sa.Column('has_at_me', mysql.TINYINT(), nullable=True))
    op.drop_column('notification', 'has_has_at')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('notification', sa.Column('has_has_at', mysql.TINYINT(display_width=4), autoincrement=False, nullable=True))
    op.drop_column('notification', 'has_at_me')
    ### end Alembic commands ###
