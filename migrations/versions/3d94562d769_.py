"""empty message

Revision ID: 3d94562d769
Revises: 13ae561ddfb0
Create Date: 2015-08-06 14:58:17.223852

"""

# revision identifiers, used by Alembic.
revision = '3d94562d769'
down_revision = '13ae561ddfb0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('T')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('T',
    sa.Column('id', mysql.INTEGER(display_width=11), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset=u'utf8',
    mysql_engine=u'InnoDB'
    )
    ### end Alembic commands ###
