"""empty message

Revision ID: 3252544a36c
Revises: 454e6e5c2e1f
Create Date: 2015-08-31 19:48:45.557948

"""

# revision identifiers, used by Alembic.
revision = '3252544a36c'
down_revision = '454e6e5c2e1f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('at_me',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('chatroom_id', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id', 'chatroom_id')
    )
    op.create_index(op.f('ix_at_me_chatroom_id'), 'at_me', ['chatroom_id'], unique=False)
    op.create_index(op.f('ix_at_me_user_id'), 'at_me', ['user_id'], unique=False)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_at_me_user_id'), table_name='at_me')
    op.drop_index(op.f('ix_at_me_chatroom_id'), table_name='at_me')
    op.drop_table('at_me')
    ### end Alembic commands ###
