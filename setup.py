from setuptools import setup, find_packages

setup(
    name='moviepp',
    version='0.1.0',
    long_description=open('README.md').read(),
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points = {
        'console_scripts': [
            'manage=pp.manage.manager:main',
        ],
    },
)
